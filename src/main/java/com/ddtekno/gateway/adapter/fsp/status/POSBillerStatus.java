package com.ddtekno.gateway.adapter.fsp.status;

import java.util.HashMap;
import java.util.Map;

import com.ddtekno.gateway.adapter.model.History;
import com.fasterxml.jackson.databind.ObjectMapper;

public class POSBillerStatus implements TransactionStatus {
	
	private String action;
	private final ObjectMapper obj = new ObjectMapper();

	@Override
	public String read(Map<String, Object> response) throws Exception {

		if (response.get("resultDesc").equals("SUKSES"))
			return "Sukses";
		else {
			action = TransactionStatus.REFUND;
			return "Gagal";
		}

	}

	@Override
	public Map<String, Object> transformResponse(Map<String, Object> response, History hist) throws Exception {
		if (response.get("type").equals("inquiry")) {
			if (hist.getPrice() > 0)
				response.put("nominal", "" + hist.getPrice());
			response.put("admin", "" + hist.getAdmin());
		}
		return response;
	}

	@Override
	public String getAction() {
		// TODO Auto-generated method stub
		return action;
	}

	@Override
	public History transformHistory(Map<String, Object> response, History hist) throws Exception {
		if(response.get("product").toString().startsWith("3009")
				&& response.get("info1") != null) {
			Map<String,String> info = new HashMap<String,String>();
			String[] data = response.get("info1").toString().split("|");
			for(String s : data) {
				if(s.startsWith("STROOM"))
					info.put("Stroom/Token", s.split(":")[1]);
				else if(s.startsWith("IDPEL"))
					info.put("No Pelanggan", s.split(":")[1]);
				else if(s.startsWith("NAMA"))
					info.put("Nama Pelanggan", s.split(":")[1]);
			}
			hist.setInfo(obj.writeValueAsString(info));
		}
		return hist;
	}

	@Override
	public Map<String,Object> settle(History hist) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
