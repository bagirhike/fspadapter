package com.ddtekno.gateway.adapter.fsp;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ThreadLocalRandom;

import com.ddtekno.gateway.adapter.model.History;
import com.ddtekno.gateway.util.HMAC;
import com.ddtekno.gateway.util.HexStringConverter;

public class POSGiroAdapterDev implements Adapter {

	private Map<String, Object> obj;
	private Map<String, String> headers;
	private Map<String, Object> data;

	private String timestamp, service, date, phoneNumber;

	@Override
	public Map<String, Object> init(String id, String orgId, String url, String service, Map<String, Object> auth) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		timestamp = sdf.format(new Date(System.currentTimeMillis()));
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		date = sdf.format(new Date(System.currentTimeMillis()));
		this.service = service;
		switch (service) {
		case "reqOTP":
			return reqOTP(id, url, service, auth);
		case "reqOTPPIN":
			return reqOTPPIN(id, url, service, auth);
		case "createAccount":
			return createAccount(id, url, service, auth);
		case "updatePIN":
			return updatePIN(id, url, service, auth);
		case "getBalance":
			return getBalance(id, url, service, auth);
		case "transfer":
			return transferP2P(id, url, service, auth);
		case "inquiry":
			return inquiry(id, url, service, auth);
		case "reversal":
			return reversal(id, url, service, auth);
		default:
			return unknownService(service);
		}
	}

	private Map<String, Object> inquiry(String id, String url, String service2, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, Object>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String clientId = (String) auth.get("client_id");
		String passwordAPI = (String) auth.get("password_api");
		String keyAPI = (String) auth.get("key_api");
		phoneNumber = getPhoneNumber((String) auth.get("chanId"));
		String signature = (String) auth.get("signature");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (clientId != null && passwordAPI != null && keyAPI != null) {
			Map<String, String> reqDocs = new HashMap<String, String>();

			data.put("account_no", phoneNumber);
			data.put("trx_date_time", timestamp);
			data.put("trx_type", "AIN");
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("client_id", clientId);
			data.put("system_trace_audit", id);
			data.put("pos_terminal_type", "5012");
			data.put("dest_account_no", props.get("send_to"));
			data.put("amount", props.get("amount"));
			data.put("description", props.get("note"));

			String key = clientId + "AIN" + phoneNumber + timestamp + id + "5012" + passwordAPI + date + keyAPI;
			if (signature == null) {
				try {
					signature = encrypt(key);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			data.put("sign", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> reversal(String id, String url, String service2, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, Object>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String clientId = (String) auth.get("client_id");
		String passwordAPI = (String) auth.get("password_api");
		String keyAPI = (String) auth.get("key_api");
		phoneNumber = getPhoneNumber((String) auth.get("chanId"));
		String signature = (String) auth.get("signature");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (clientId != null && passwordAPI != null && keyAPI != null) {
			Map<String, String> reqDocs = new HashMap<String, String>();

			data.put("account_no", phoneNumber);
			data.put("trx_date_time", timestamp);
			data.put("trx_type", "REV");
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("client_id", clientId);
			data.put("system_trace_audit", id);
			data.put("pos_terminal_type", "5012");
			data.put("amount", props.get("amount"));

			String key = clientId + "REV" + phoneNumber + timestamp + id + "5012" + passwordAPI + date + keyAPI;
			if (signature == null) {
				try {
					signature = encrypt(key);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			data.put("sign", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> transferP2P(String id, String url, String service2, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, Object>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("parser", "json");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		History hist = new History();
		String clientId = (String) auth.get("client_id");
		String passwordAPI = (String) auth.get("password_api");
		String keyAPI = (String) auth.get("key_api");
		phoneNumber = getPhoneNumber((String) auth.get("chanId"));
		String signature = (String) auth.get("signature");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (clientId != null && passwordAPI != null && keyAPI != null
				&& props != null && props.containsKey("send_to") 
				&& props.containsKey("amount") && props.containsKey("note")) {
			Map<String, String> reqDocs = new HashMap<String, String>();

			data.put("account_no", phoneNumber);
			data.put("trx_date_time", timestamp);
			data.put("trx_type", "TRF");
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("client_id", clientId);
			data.put("system_trace_audit", id);
			data.put("pos_terminal_type", "5012");
			data.put("dest_account_no", props.get("send_to"));
			data.put("amount", props.get("amount"));
			data.put("description", props.get("note"));

			String key = clientId + "TRF" + phoneNumber + timestamp + id + "5012" + passwordAPI + date + keyAPI;
			if (signature == null) {
				try {
					signature = encrypt(key);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else {
				hist.setReffNo(id);
				hist.setProduct("TRANSFER TO "+props.get("send_to"));
				hist.setDesc(props.get("note"));
				hist.setTimestamp(timestamp);
				hist.setChannelId(phoneNumber);
				hist.setPrice(0);
				hist.setAdmin(0);
				hist.setTotal(Integer.parseInt(props.get("amount")));
				hist.setHistorian("id.co.hike.gateway.adapter.fsp.status.POSGiroStatus");
				obj.put("history", hist);
			}
			
			data.put("sign", signature);
			obj.put("data", data);
			
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> getBalance(String id, String url, String service2, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, Object>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String clientId = (String) auth.get("client_id");
		String passwordAPI = (String) auth.get("password_api");
		String keyAPI = (String) auth.get("key_api");
		phoneNumber = getPhoneNumber((String) auth.get("chanId"));
		String signature = (String) auth.get("signature");

		if (clientId != null && passwordAPI != null && keyAPI != null) {

			data.put("account_no", phoneNumber);
			data.put("trx_date_time", timestamp);
			data.put("trx_type", "BIN");
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("client_id", clientId);
			data.put("system_trace_audit", id);
			data.put("pos_terminal_type", "6022");
			String key = clientId + "BIN" + phoneNumber + timestamp + id + "5012" + passwordAPI + date + keyAPI;
			if (signature == null) {
				try {
					signature = encrypt(key);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			data.put("sign", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> updatePIN(String id, String url, String service2, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, Object>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String clientId = (String) auth.get("client_id");
		String passwordAPI = (String) auth.get("password_api");
		String keyAPI = (String) auth.get("key_api");
		phoneNumber = getPhoneNumber((String) auth.get("chanId"));
		String signature = (String) auth.get("signature");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		
		if (clientId != null && passwordAPI != null && keyAPI != null) {
			Map<String, String> reqDocs = new HashMap<String, String>();

			data.put("trx_date_time", timestamp);
			data.put("trx_type", "CRP");
			data.put("account_no", phoneNumber);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("client_id", clientId);
			data.put("system_trace_audit", id);
			data.put("pos_terminal_type", "5012");
			data.put("otp", props.get("otp"));
			data.put("jenis_crp", props.get("action"));

			String key = clientId + "CRP" + phoneNumber + timestamp + id + "5012" + passwordAPI + date + keyAPI;
			if (signature == null) {
				try {
					signature = encrypt(key);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			data.put("sign", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> reqOTP(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, Object>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String clientId = (String) auth.get("client_id");
		String passwordAPI = (String) auth.get("password_api");
		String keyAPI = (String) auth.get("key_api");
		phoneNumber = getPhoneNumber((String) auth.get("chanId"));
		String signature = (String) auth.get("signature");
		
		if (clientId != null && passwordAPI != null && keyAPI != null) {

			data.put("telepon_hp_nomor", phoneNumber);
			data.put("trx_date_time", timestamp);
			data.put("trx_type", "OTR");
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("client_id", clientId);
			data.put("system_trace_audit", id);
			data.put("pos_terminal_type", "5012");
			data.put("trx_code", "CRT");
			String key = clientId + "OTR" + phoneNumber + timestamp + id + "5012" + passwordAPI + date + keyAPI;
			if (signature == null) {
				try {
					signature = encrypt(key);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			data.put("sign", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}
	
	private Map<String, Object> reqOTPPIN(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, Object>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String clientId = (String) auth.get("client_id");
		String passwordAPI = (String) auth.get("password_api");
		String keyAPI = (String) auth.get("key_api");
		phoneNumber = getPhoneNumber((String) auth.get("chanId"));
		String signature = (String) auth.get("signature");
		
		if (clientId != null && passwordAPI != null && keyAPI != null) {

			data.put("account_no", phoneNumber);
			data.put("trx_date_time", timestamp);
			data.put("trx_type", "OTR");
			data.put("jenis_kirim", "M");
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("client_id", clientId);
			data.put("system_trace_audit", id);
			data.put("pos_terminal_type", "5012");
			String key = clientId + "OTR" + phoneNumber + timestamp + id + "5012" + passwordAPI + date + keyAPI;
			if (signature == null) {
				try {
					signature = encrypt(key);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			data.put("sign", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> createAccount(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, Object>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String clientId = (String) auth.get("client_id");
		String passwordAPI = (String) auth.get("password_api");
		String keyAPI = (String) auth.get("key_api");
		phoneNumber = getPhoneNumber((String) auth.get("chanId"));
		String signature = (String) auth.get("signature");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (clientId != null && passwordAPI != null && keyAPI != null) {
			Map<String, String> reqDocs = new HashMap<String, String>();

			data.put("trx_date_time", timestamp);
			data.put("trx_type", "CRT");
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("client_id", clientId);
			data.put("system_trace_audit", id);
			data.put("pos_terminal_type", "5012");
			data.put("otp_code", props.get("otp"));
			reqDocs.put("request_date", date);
			reqDocs.put("tanggal_buka", date);
			reqDocs.put("nama_nasabah", props.get("nama_nasabah"));
			reqDocs.put("nama_singkat", props.get("nama_singkat"));
			reqDocs.put("telepon_hp_nomor", phoneNumber);
			data.put("request_docs", reqDocs);

			String key = clientId + "CTR" + phoneNumber + timestamp + id + "5012" + passwordAPI + date + keyAPI;
			if (signature == null) {
				try {
					signature = encrypt(key);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			data.put("sign", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> unknownService(String service) {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message", "Unknown service requested " + service);
		return obj;
	}

	private String encrypt(String val) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(val.getBytes(StandardCharsets.UTF_8));
		byte byteData[] = md.digest();
		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
	
	private String getPhoneNumber(String string) {
		return string.split("@")[0];
	}

	@Override
	public String getTimestamp() {
		// TODO Auto-generated method stub
		return timestamp;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return phoneNumber;
	}

	@Override
	public String getService() {
		// TODO Auto-generated method stub
		return this.service;
	}

	@Override
	public String getFSP() {
		// TODO Auto-generated method stub
		return "sof-giro-pos";
	}

}
