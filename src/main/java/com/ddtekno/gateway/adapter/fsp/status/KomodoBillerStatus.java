package com.ddtekno.gateway.adapter.fsp.status;

import java.util.HashMap;
import java.util.Map;

import com.ddtekno.gateway.adapter.model.History;

public class KomodoBillerStatus implements TransactionStatus {

	private String action;
	
	@Override
	public String read(Map<String, Object> response) throws Exception {
		
		if(response.containsKey("rc") && response.get("rc").equals("00"))
			return "Sukses";
		if(response.containsKey("rc") && 
				response.get("rc").equals("68") || response.get("rc").equals("96"))
			return "Pending";
		else {
			action = TransactionStatus.REFUND;
			return "Gagal";
		}
			
	}

	@Override
	public Map<String, Object> transformResponse(Map<String, Object> response, History hist) throws Exception {
		// TODO Auto-generated method stub
		return response;
	}

	@Override
	public String getAction() {
		// TODO Auto-generated method stub
		return action;
	}

	@Override
	public History transformHistory(Map<String, Object> response, History hist) throws Exception {
		
		return hist;
	}

	@Override
	public Map<String,Object> settle(History hist) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
