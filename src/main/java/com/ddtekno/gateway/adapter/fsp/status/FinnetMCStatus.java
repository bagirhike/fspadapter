package com.ddtekno.gateway.adapter.fsp.status;

import java.util.Map;

import com.ddtekno.gateway.adapter.model.Gateway;
import com.ddtekno.gateway.adapter.model.History;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FinnetMCStatus implements TransactionStatus {

	private String action;

	private final ObjectMapper obj = new ObjectMapper();

	@Override
	public String read(Map<String, Object> response) throws Exception {
		if (response.get("statusDesc").equals("Success")) {
			action = TransactionStatus.BUY;
			return "Sukses";
		} else
			return "Gagal";

	}

	@Override
	public Map<String, Object> transformResponse(Map<String, Object> response, History hist) throws Exception {
		// TODO Auto-generated method stub
		return response;
	}

	@Override
	public String getAction() {
		// TODO Auto-generated method stub
		return action;
	}

	@Override
	public History transformHistory(Map<String, Object> response, History hist) throws Exception {
		if (hist.getMessageSend() != null) {
			Gateway g = obj.readValue(hist.getMessageSend(), Gateway.class);
			g.getRefund().getProps().put("appCode", response.get("appCode"));
			hist.setMessageSend(obj.writeValueAsString(g));
		}
		return hist;
	}

	@Override
	public Map<String,Object> settle(History hist) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
