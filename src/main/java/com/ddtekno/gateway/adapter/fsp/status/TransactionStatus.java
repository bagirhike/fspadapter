package com.ddtekno.gateway.adapter.fsp.status;

import java.util.Map;

import com.ddtekno.gateway.adapter.model.History;

public interface TransactionStatus {
	
	public static String SUKSES = "Sukses";
	public static String GAGAL = "Gagal";
	public static String PENDING = "Pending";
	
	public static String BUY = "buy";
	public static String REFUND = "refund";
	
	public String read(Map<String,Object> response) throws Exception;
	
	public Map<String,Object> settle(History hist) throws Exception;
	
	public String getAction();
	
	public History transformHistory(Map<String,Object> response, History hist) throws Exception;
	
	public Map<String,Object> transformResponse(Map<String,Object> response, History hist) throws Exception;

}
