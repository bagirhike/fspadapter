package com.ddtekno.gateway.adapter.fsp;

import java.util.Map;


public interface Adapter {
	public String getTimestamp();
	public String getId();
	public String getService();
	public String getFSP();
	public Map<String,Object> init(String id, String orgId, String url, String service, Map<String,Object> auth);
	
}
