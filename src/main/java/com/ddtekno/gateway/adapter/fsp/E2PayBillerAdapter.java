package com.ddtekno.gateway.adapter.fsp;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.bson.internal.Base64;

import com.ddtekno.gateway.util.HMAC;
import com.ddtekno.soap.e2pay.WsbgRequest;

public class E2PayBillerAdapter implements Adapter {

	private Map<String, Object> obj;
	private Map<String, String> headers;
	private Map<String, String> data;

	private String timestampAuth;
	private Calendar timestamp;

	@Override
	public Map<String, Object> init(String id, String orgId, String url, String service, Map<String, Object> auth) {
		data = new HashMap<String, String>();
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss zzz");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		timestampAuth = sdf.format(date).replaceAll("WIB", "ICT");
		
		sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		timestamp = Calendar.getInstance(TimeZone.getTimeZone("Asia/Jakarta"));
		sdf.setCalendar(timestamp);

		switch (service) {
		case "purchase":
			return purchase(id, url, service, auth);
		case "checkPurchase":
			return checkPurchase(id, url, service, auth);
		default:
			return unknownService(service);
		}
	}

	private Map<String, Object> unknownService(String service) {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message", "Unknown service requested " + service);
		return obj;
	}

	private Map<String, Object> purchase(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/soap+xml");
		headers.put("type", "soap");

		String bankId = (String) auth.get("BankID");
		String bankChannel = (String) auth.get("ChannelID");
		String secret = (String) auth.get("SecretKey");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (bankId != null && bankChannel != null && secret != null && props != null) {

			String stringtoSign = service + "\n" + bankId + "\n" + props.get("refNo") + "\n" + bankChannel + "\n"
					+ props.get("custId") + "\n" + timestampAuth;
			
			try {
				String token = encrypt(stringtoSign, secret);
				headers.put("signature", token);
				headers.put("timeauth", timestampAuth);

				WsbgRequest request = new WsbgRequest();
				request.setBankId(bankId);
				request.setBankChannel(bankChannel);
				request.setBankRefNo(props.get("refNo"));
				request.setCustAccNo((String) auth.get("chanId"));
				request.setCustId(props.get("custId"));
				request.setPayeeCode(props.get("payeeCode"));
				request.setProductCode(props.get("productCode"));
				request.setDateTrx(timestamp);

				obj.put("headers", headers);
				obj.put("data", request);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				obj.put("message", "Encryption Error");
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				obj.put("message", "Key Error");
			}
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> checkPurchase(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/soap+xml");
		headers.put("type", "soap");

		String bankId = (String) auth.get("BankID");
		String bankChannel = (String) auth.get("ChannelID");
		String secret = (String) auth.get("SecretKey");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (bankId != null && bankChannel != null && secret != null && props != null) {

			String stringtoSign = service + "\n" + bankId + "\n" + "" + "\n" + "" + "\n"
					+ props.get("custId") + "\n" + timestampAuth;
			
			try {
				String token = encrypt(stringtoSign, secret);
				headers.put("signature", token);
				headers.put("timeauth", timestampAuth);

				WsbgRequest request = new WsbgRequest();
				request.setBankId(bankId);
				request.setCustId(props.get("custId"));
				request.setPayeeCode(props.get("payeeCode"));
				request.setProductCode(props.get("productCode"));
				request.setCustRefNo(props.get("refNo"));

				obj.put("headers", headers);
				obj.put("data", request);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				obj.put("message", "Encryption Error");
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				obj.put("message", "Key Error");
			}
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private String encrypt(String sign,String secret) throws NoSuchAlgorithmException, InvalidKeyException {
		
		Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
	     SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
	     sha256_HMAC.init(secret_key);

	     String hash = Base64.encode(sha256_HMAC.doFinal(sign.getBytes()));
	     
	     return hash;
	}

	@Override
	public String getTimestamp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getFSP() {
		// TODO Auto-generated method stub
		return null;
	}

}
