package com.ddtekno.gateway.adapter.fsp;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.bson.internal.Base64;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CIMBNiagaAPIAdapter implements Adapter {

	private Map<String, Object> obj;
	private Map<String, String> headers;
	private Map<String, Object> data;

	private String timestamp, service;
	
	private final ObjectMapper mapper = new ObjectMapper();

	@Override
	public Map<String, Object> init(String id, String orgId, String url, String service, Map<String, Object> auth) {
		data = new HashMap<String, Object>();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		timestamp = sdf.format(new Date(System.currentTimeMillis()));
		
		this.service = service;
		switch (service) {
		case "inquiry":
			return inquiry(id, url, service, auth);
		case "trx":
			return trx(id, url, service, auth);
		case "list":
			return listProduct(id, url, service, auth);
		case "info":
			return infoProduct(id, url, service, auth);
		default:
			return unknownService(service);
		}
	}
	
	private Map<String, Object> listProduct(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		obj.put("method", "GET");

		
		String apikey = (String) auth.get("apikey");
		String secret = (String) auth.get("secret");
		Map<String, Object> props = (Map<String, Object>) auth.get("props");
		if (apikey != null && secret != null && props != null && props.containsKey("noRef")) {
			
			headers.put("CIMB-ApiKey", apikey);
			headers.put("CIMB-Service", "B2B");
			headers.put("CIMB-Timestamp", timestamp);
			
			data.put("referenceNo", props.get("noRef").toString());
			obj.put("url", "https://apihub.cimbniaga.co.id:8043/api-manager-external/production/sandbox/product/product/category/list?"
					+ "referenceNo="+props.get("noRef").toString());
			
			
			try {
				
				String body = mapper.writeValueAsString(data).replaceAll("[\n\r\t]", "").replaceAll("\\s", "");
				String bodyenc = Base64.encode(MessageDigest.getInstance("MD5").digest(body.getBytes("UTF-8")));
				
				String string = timestamp+":"+apikey+":GET:"+bodyenc;
				
				SecretKeySpec spec = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256");
				Mac hmac = Mac.getInstance("HmacSHA256");
				hmac.init(spec);
				
				String signBase64 = apikey+":"+Base64.encode(hmac.doFinal(string.getBytes("UTF-8")));
				
				headers.put("CIMB-Signature", Base64.encode(signBase64.getBytes("UTF-8")));
				
			} catch (JsonProcessingException e) {
				obj.put("message", "Error in parsing JSON");
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				obj.put("message", "Error in parsing JSON - Alghorithm");
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				obj.put("message", "Error in parsing JSON - Encoding");
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				obj.put("message", "Error in parsing JSON - Key");
				e.printStackTrace();
			}
			
			
			obj.put("headers", headers);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}
	
	private Map<String, Object> infoProduct(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		obj.put("url", "https://apihub.cimbniaga.co.id:8043/api-manager-external/production/sandbox/product/product/info");
		obj.put("method", "POST");
		headers.put("contentType", "application/json");

		
		String apikey = (String) auth.get("apikey");
		String secret = (String) auth.get("secret");
		Map<String, Object> props = (Map<String, Object>) auth.get("props");
		if (apikey != null && secret != null && props != null && props.containsKey("noRef")) {
			
			headers.put("CIMB-ApiKey", apikey);
			headers.put("CIMB-Service", "B2B");
			headers.put("CIMB-Timestamp", timestamp);
			
			data.put("referenceNo", props.get("noRef").toString());
			data.put("productCategory", props.get("category").toString());
			data.put("productClass", props.get("class").toString());
			
			try {
				
				String body = mapper.writeValueAsString(data).replaceAll("[\n\r\t]", "").replaceAll("\\s", "");
				String bodyenc = Base64.encode(MessageDigest.getInstance("MD5").digest(body.getBytes("UTF-8")));
				
				String string = timestamp+":"+apikey+":POST:"+bodyenc;
				
				SecretKeySpec spec = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256");
				Mac hmac = Mac.getInstance("HmacSHA256");
				hmac.init(spec);
				
				String signBase64 = apikey+":"+Base64.encode(hmac.doFinal(string.getBytes("UTF-8")));
				
				headers.put("CIMB-Signature", Base64.encode(signBase64.getBytes("UTF-8")));
				
			} catch (JsonProcessingException e) {
				obj.put("message", "Error in parsing JSON");
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				obj.put("message", "Error in parsing JSON - Alghorithm");
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				obj.put("message", "Error in parsing JSON - Encoding");
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				obj.put("message", "Error in parsing JSON - Key");
				e.printStackTrace();
			}
			
			
			obj.put("headers", headers);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> inquiry(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		obj.put("url", url+service);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");

		
		String apikey = (String) auth.get("apikey");
		String secret = (String) auth.get("secret");
		Map<String, Object> props = (Map<String, Object>) auth.get("props");
		if (apikey != null && secret != null && props != null && props.containsKey("customerNo")) {
			
			headers.put("CIMB-ApiKey", apikey);
			headers.put("CIMB-Service", "B2B");
			headers.put("CIMB-Timestamp", timestamp);
			
			data.put("referenceNo", props.get("noRef").toString());
			data.put("companyCode", props.get("billerCode").toString());
			data.put("customerNo", props.get("customerNo").toString());
			
			try {
				
				String body = mapper.writeValueAsString(data).replaceAll("[\n\r\t]", "").replaceAll("\\s", "");
				String bodyenc = Base64.encode(MessageDigest.getInstance("MD5").digest(body.getBytes("UTF-8")));
				
				String string = timestamp+":"+apikey+":POST:"+bodyenc;
				
				SecretKeySpec spec = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256");
				Mac hmac = Mac.getInstance("HmacSHA256");
				hmac.init(spec);
				
				String signBase64 = apikey+":"+Base64.encode(hmac.doFinal(string.getBytes("UTF-8")));
				
				headers.put("CIMB-Signature", Base64.encode(signBase64.getBytes("UTF-8")));
				
			} catch (JsonProcessingException e) {
				obj.put("message", "Error in parsing JSON");
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				obj.put("message", "Error in parsing JSON - Alghorithm");
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				obj.put("message", "Error in parsing JSON - Encoding");
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				obj.put("message", "Error in parsing JSON - Key");
				e.printStackTrace();
			}
			
			
			obj.put("headers", headers);
			obj.put("data", data);
			try {
				System.out.println(mapper.writeValueAsString(data));
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}
	
	private Map<String, Object> trx(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		obj.put("url", url+service);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");;

		
		String apikey = (String) auth.get("apikey");
		String secret = (String) auth.get("secret");
		Map<String, Object> props = (Map<String, Object>) auth.get("props");
		if (apikey != null && secret != null && props != null && props.containsKey("customerNo")) {
			
			headers.put("CIMB-ApiKey", apikey);
			headers.put("CIMB-Service", "B2B");
			headers.put("CIMB-Timestamp", timestamp);
			
			data.put("referenceNo", props.get("noRef").toString());
			data.put("name", props.get("name").toString());
			data.put("amount", (Integer)props.get("amount"));
			data.put("companyCode", props.get("billerCode").toString());
			data.put("customerNo", props.get("customerNo").toString());
			
			try {
				
				String body = mapper.writeValueAsString(data).replaceAll("[\n\r\t]", "").replaceAll("\\s", "");
				String bodyenc = Base64.encode(MessageDigest.getInstance("MD5").digest(body.getBytes("UTF-8")));
				
				String string = timestamp+":"+apikey+":POST:"+bodyenc;
				
				SecretKeySpec spec = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256");
				Mac hmac = Mac.getInstance("HmacSHA256");
				hmac.init(spec);
				
				String signBase64 = apikey+":"+Base64.encode(hmac.doFinal(string.getBytes("UTF-8")));
				
				headers.put("CIMB-Signature", Base64.encode(signBase64.getBytes("UTF-8")));
				
			} catch (JsonProcessingException e) {
				obj.put("message", "Error in parsing JSON");
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				obj.put("message", "Error in parsing JSON - Alghorithm");
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				obj.put("message", "Error in parsing JSON - Encoding");
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				obj.put("message", "Error in parsing JSON - Key");
				e.printStackTrace();
			}
			
			
			obj.put("headers", headers);
			obj.put("data", data);
			try {
				System.out.println(mapper.writeValueAsString(data));
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> unknownService(String service) {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message", "Unknown service requested " + service);
		return obj;
	}

	@Override
	public String getTimestamp() {
		// TODO Auto-generated method stub
		return timestamp;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return data.get("phoneNumber").toString();
	}

	@Override
	public String getService() {
		// TODO Auto-generated method stub
		return this.service;
	}

	@Override
	public String getFSP() {
		// TODO Auto-generated method stub
		return "pg-nicepay";
	}

}
