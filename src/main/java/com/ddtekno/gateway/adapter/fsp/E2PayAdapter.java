package com.ddtekno.gateway.adapter.fsp;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.bson.internal.Base64;

import com.ddtekno.gateway.adapter.model.History;

public class E2PayAdapter implements Adapter {

	private Map<String, Object> obj;
	private Map<String, String> headers;
	private Map<String, String> data;
	
	private String service;

	@Override
	public Map<String, Object> init(String id, String orgId, String url, String service, Map<String, Object> auth) {
		data = new HashMap<String, String>();
		
		this.service = service;
		switch (service) {
		case "requestPayment":
			return requestPayment(id, url, service, auth);
		default:
			return unknownService(service);
		}
	}

	private Map<String, Object> unknownService(String service) {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message", "Unknown service requested " + service);
		return obj;
	}

	private Map<String, Object> requestPayment(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("type", "xform");
		headers.put("contentType", "application/x-www-form-urlencoded");
		obj.put("headers", headers);
		
		data.put("Currency", "IDR");
		data.put("UserName", "muhbagir");
		data.put("UserEmail", "muhbagir@gmail.com");
		data.put("ResponseURL", "http://34.87.70.137/callback");
		data.put("BackendURL", "http://34.87.70.137/callback");
		data.put("PaymentId", "10");
		

		String mcode = (String) auth.get("MerchantCode");
		String mkey = (String) auth.get("MerchantKey");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (mcode != null && mkey != null && props != null && props.containsKey("UserContact")
				 && props.containsKey("Amount") && props.containsKey("RefNo") 
				 && props.containsKey("ProdDesc")) {
			
			String key = mkey + mcode + props.get("RefNo") + props.get("Amount") + "IDR";
			try {
				String token = encrypt(key);
				data.put("MerchantCode", mcode);
				data.put("Signature", token);
				data.putAll(props);
				String body = data.toString().trim();
				body = body.substring(1, body.length()-1).replace("\"", "").replace(", ", "&");
				obj.put("data", body);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				obj.put("message", "Encryption Error");
			}
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}
	
	private String encrypt(String val) throws NoSuchAlgorithmException {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(val.getBytes(StandardCharsets.UTF_8));
			byte byteData[] = md.digest();
			
			return Base64.encode(byteData);
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return data.get("phoneNumber");
	}

	@Override
	public String getService() {
		// TODO Auto-generated method stub
		return this.service;
	}

	@Override
	public String getFSP() {
		// TODO Auto-generated method stub
		return "pg-nicepay";
	}

	@Override
	public String getTimestamp() {
		// TODO Auto-generated method stub
		return null;
	}

}
