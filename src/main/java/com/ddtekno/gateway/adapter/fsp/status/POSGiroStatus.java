package com.ddtekno.gateway.adapter.fsp.status;

import java.util.Map;

import com.ddtekno.gateway.adapter.model.History;

public class POSGiroStatus implements TransactionStatus {

	private String action;
	
	@Override
	public String read(Map<String, Object> response) throws Exception {
		if(response.get("response_msg").toString().equalsIgnoreCase("success"))
			return "Sukses";
		else {
			action = "posdev";
			return "Gagal";
		}
			
	}

	@Override
	public Map<String, Object> transformResponse(Map<String, Object> response, History hist) throws Exception {
		// TODO Auto-generated method stub
		return response;
	}

	@Override
	public String getAction() {
		// TODO Auto-generated method stub
		return action;
	}

	@Override
	public History transformHistory(Map<String, Object> response, History hist) throws Exception {
		// TODO Auto-generated method stub
		return hist;
	}

	@Override
	public Map<String,Object> settle(History hist) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
