package com.ddtekno.gateway.adapter.fsp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.codec.digest.DigestUtils;

import com.ddtekno.soap.finnet.InputCheck;

public class FinnetRitnasAdapter implements Adapter {

	private Map<String, Object> obj;
	private Map<String, String> headers;
	private Map<String, String> data;

	
	private String timestamp;

	@Override
	public Map<String, Object> init(String id, String orgId, String url, String service, Map<String, Object> auth) {
		data = new HashMap<String, String>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		timestamp = sdf.format(new Date());

		switch (service) {
		case "billpayment":
			return billpayment(id, url, service, auth);
		default:
			return unknownService(service);
		}
	}

	private Map<String, Object> unknownService(String service) {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message", "Unknown service requested " + service);
		return obj;
	}

	private Map<String, Object> billpayment(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/soap+xml");
		headers.put("type", "soap");

		String username = (String) auth.get("username");
		String password = (String) auth.get("password");
		String merchantCode = (String) auth.get("merchantCode");
		String merchantNumber = (String) auth.get("merchantNumber");
		String terminal = (String) auth.get("terminal");
		String branchCode = (String) auth.get("branchCode");
		String locationCode = (String) auth.get("locationCode");
		String productCode = (String) auth.get("productCode");
		Map<String, Object> props = (Map<String, Object>) auth.get("props");
		if (username != null && password != null && props != null) {

			password = DigestUtils.sha256Hex(password);
			String stringtoSign = username + "." + password + "." + merchantNumber + "." + productCode + "."
					+ merchantCode + "." + terminal + ".50" + ".0" + props.get("noHP") + "." + props.get("prodPrice")
					+ "." + props.get("trxId") + "." + timestamp +".TELKOMSEL";
			
			String token = DigestUtils.sha1Hex(stringtoSign);
			

			InputCheck input = new InputCheck();
			input.setUserName(username);
			input.setSignature(token);
			input.setMerchantNumber(merchantNumber);
			input.setMerchantCode(merchantCode);
			input.setProductCode(productCode);
			input.setTerminal(terminal);
			input.setBranchCode(branchCode);
			input.setLocationCode(locationCode);
			input.setTransactionType("50");
			input.setBillNumber("0"+(String)props.get("noHP"));
			//int total = Integer.parseInt(props.get("prodPrice")) + Integer.parseInt(props.get("prodAdmin"));
			input.setAmount(String.valueOf(props.get("prodPrice")));
			input.setBit61("0"+(String)props.get("noHP"));
			input.setTraxId((String)props.get("trxId"));
			input.setTimeStamp(timestamp);
			input.setPacketType("TELKOMSEL");
			
			obj.put("headers", headers);
			obj.put("data", input);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	
	@Override
	public String getTimestamp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getFSP() {
		// TODO Auto-generated method stub
		return null;
	}

}
