package com.ddtekno.gateway.adapter.fsp;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import com.ddtekno.gateway.adapter.model.History;

public class NicePayAdapter implements Adapter {

	private Map<String, Object> obj;
	private Map<String, String> headers;
	private Map<String, String> data;
	
	private String fsp, organization, timestamp, service;

	@Override
	public Map<String, Object> init(String id, String orgId, String url, String service, Map<String, Object> auth) {
		data = new HashMap<String, String>();
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		timestamp = sdf.format(new Date(System.currentTimeMillis()));
		this.service = service;
		this.fsp = id;
		this.organization = orgId;
		switch (service) {
		case "registrationOVO":
			return registrationOVO(id, url, service, auth);
		case "paymentOVO":
			return paymentOVO(id, url, service, auth);
		default:
			return unknownService(service);
		}
	}

	private Map<String, Object> paymentOVO(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("type", "xform");
		headers.put("parser", "html");
		headers.put("contentType", "application/x-www-form-urlencoded");
		obj.put("headers", headers);
		data.put("callBackUrl", "http://34.87.70.137/callback");
		
		History hist = new History();
		String mkey = (String) auth.get("mkey");
		String mid = (String) auth.get("mid");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (mid != null && mkey != null && props != null && props.containsKey("tXid")
				 && props.containsKey("amt") && props.containsKey("referenceNo")
				 && props.containsKey("merchantId")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
			String current = sdf.format(new Date(System.currentTimeMillis()));
			data.put("timeStamp", current);
			String key = current + mid + props.get("referenceNo") + props.get("amt") + mkey;
			try {
				String token = encrypt(key);
				data.put("merchantToken", token);
				data.putAll(props);
				String body = data.toString().trim();
				body = body.substring(1, body.length()-1).replace("\"", "").replace(", ", "&");
				obj.put("data", body);
				
				hist.setReffNo(props.get("referenceNo"));
				hist.setTimestamp(timestamp);
				hist.setChannelId(props.get("merchantId"));
				hist.setPrice(Integer.parseInt(props.get("amt")));
				hist.setTotal(Integer.parseInt(props.get("amt")));
				hist.setFsp(fsp);
				hist.setOrganization(organization);
				hist.setHistorian("id.co.hike.gateway.adapter.fsp.status.NicePayStatus");
				
				if(auth.containsKey("settlement"))
					hist.setSettlement((Map<String,Object>)auth.get("settlement"));
				
				obj.put("history", hist);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				obj.put("message", "Encryption Error");
			}
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> unknownService(String service) {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message", "Unknown service requested " + service);
		return obj;
	}

	private Map<String, Object> registrationOVO(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);
		data.put("billingCity", "Jakarta");
		data.put("billingCountry", "ID");
		data.put("billingEmail", "ikasir@gmail.com");
		data.put("billingNm", "IKasir");
		data.put("billingPostCd", "12950");
		data.put("billingState", "Jakarta");
		data.put("cartData", "{\"count\": \"1\",\"item\": [{\"img_url\": \"https://www.lecs.com/image/introduction/img_vmd020101.jpg\",\"goods_name\": \"Jam Tangan Army - Strap Kulit - Hitam\",\"goods_detail\": \"jumlah 1\",\"goods_amt\": \"400\"}]}");
		data.put("currency", "IDR");
		data.put("dbProcessUrl", "http://34.87.70.137/callback");
		data.put("goodsNm", "ikasir-item");
		data.put("userIP", "127.0.0.1");
		data.put("deliveryNm", "");
		data.put("deliveryPhone", "");
		data.put("deliveryAddr", "");
		data.put("deliveryCity", "");
		data.put("deliveryState", "");
		data.put("deliveryPostCd", "");
		data.put("deliveryCountry", "");
		data.put("vat", "");
		data.put("fee", "");
		data.put("notaxAmt", "");
		data.put("description", "description");
		data.put("reqDt", "");
		data.put("reqTm", "");
		data.put("reqDomain", "");
		data.put("reqServerIP", "");
		data.put("reqClientVer", "");
		data.put("userSessionID", "");
		data.put("userAgent", "");
		data.put("userLanguage", "");
		data.put("instmntType", "");
		data.put("instmntMon", "");
		data.put("recurrOpt", "");
		data.put("bankCd", "");
		data.put("vacctValidDt", "");
		data.put("vacctValidTm", "");
		data.put("merFixAcctId", "");
		data.put("payValidDt", "");
		data.put("payValidTm", "");
		data.put("mRefNo", "");
		data.put("mitraCd", "OVOE");
		data.put("payMethod", "05");

		String mkey = (String) auth.get("mkey");
		String mid = (String) auth.get("mid");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (mid != null && mkey != null && props != null && props.containsKey("billingPhone")
				 && props.containsKey("amt") && props.containsKey("referenceNo")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
			String current = sdf.format(new Date(System.currentTimeMillis()));
			data.put("timeStamp", current);
			String key = current + mid + props.get("referenceNo") + props.get("amt") + mkey;
			try {
				String token = encrypt(key);
				data.put("merchantToken", token);
				data.put("iMid", mid);
				data.putAll(props);
				obj.put("data", data);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				obj.put("message", "Encryption Error");
			}
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}
	
	private String encrypt(String val) throws NoSuchAlgorithmException {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(val.getBytes(StandardCharsets.UTF_8));
			byte byteData[] = md.digest();
			// convert the byte to hex format method 1
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
	}
	
	@Override
	public String getTimestamp() {
		// TODO Auto-generated method stub
		return timestamp;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return data.get("phoneNumber");
	}

	@Override
	public String getService() {
		// TODO Auto-generated method stub
		return this.service;
	}

	@Override
	public String getFSP() {
		// TODO Auto-generated method stub
		return "pg-nicepay";
	}

}
