package com.ddtekno.gateway.adapter.fsp.status;

import java.util.Map;

import org.json.JSONObject;

import com.ddtekno.gateway.adapter.model.History;

public class NicePayStatus implements TransactionStatus {

	private JSONObject json;
	
	@Override
	public String read(Map<String, Object> response) throws Exception {
		
		if(response.containsKey("resultMsg") && response.get("resultMsg").equals("SUCCESS"))
			return "Sukses";
		else
			return "Gagal";
			
	}

	@Override
	public Map<String, Object> transformResponse(Map<String, Object> response, History hist) throws Exception {
		// TODO Auto-generated method stub
		return response;
	}

	@Override
	public String getAction() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public History transformHistory(Map<String, Object> response, History hist) throws Exception {
		// TODO Auto-generated method stub
		return hist;
	}

	@Override
	public Map<String,Object> settle(History hist) throws Exception {
		json = new JSONObject(hist.getSettlement());
		
		for(String key : json.keySet()) {
			double p = json.getDouble(key);
			json.put(key,((p/100)*hist.getTotal()));
		}
		
		return json.toMap();
	}

}
