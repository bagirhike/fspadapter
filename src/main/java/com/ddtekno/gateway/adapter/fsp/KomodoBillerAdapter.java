package com.ddtekno.gateway.adapter.fsp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ThreadLocalRandom;

import com.ddtekno.gateway.adapter.model.History;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class KomodoBillerAdapter implements Adapter {

	private Map<String, Object> obj;
	private Map<String, String> headers;
	private Map<String, String> data;

	private String timestamp, service, fsp, org;
	
	private final ObjectMapper mapper = new ObjectMapper();

	@Override
	public Map<String, Object> init(String id, String orgId, String url, String service, Map<String, Object> auth) {
		data = new HashMap<String, String>();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		timestamp = sdf.format(new Date(System.currentTimeMillis()));
		fsp = id;
		org = orgId;
		this.service = service;
		switch (service) {
		case "requestBuy":
			return requestBuy(id, url, service, auth);
		default:
			return unknownService(service);
		}
	}

	private Map<String, Object> requestBuy(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		obj.put("url", url);
		obj.put("method", "GET");
		headers.put("parser", "json");

		History hist = new History();
		String terminal = (String) auth.get("terminal");
		String password = (String) auth.get("password");
		Map<String, Object> props = (Map<String, Object>) auth.get("props");
		if (terminal != null && password != null && props != null && props.containsKey("prodId")
				&& props.containsKey("destination")) {
			
			if(props.containsKey("request_id"))
				id = props.get("request_id").toString();
			else
				id = "" + ThreadLocalRandom.current().nextInt(1000000);
			
			data.put("request_id", id);
			data.put("terminal_name", terminal);
			data.put("password", password);
			data.put("terminal_name", terminal);
			if(props.containsKey("callback"))
				data.put("reverse_url", props.get("callback").toString());
			else
				data.put("reverse_url", "http://34.87.70.137/callback/komodo");
			data.put("product_name", props.get("prodId").toString());
			data.put("destination", props.get("destination").toString());

			String body = data.toString().trim();
			body = body.substring(1, body.length() - 1).replace("\"", "").replace(", ", "&");
			headers.put("query", body);
			obj.put("headers", headers);

			hist.setReffNo(id);
			hist.setTimestamp(timestamp);
			hist.setChannelId(terminal);
			hist.setProduct(props.get("prodName").toString());
			hist.setPrice((Integer)props.get("prodPrice"));
			hist.setAdmin((Integer)props.get("prodAdmin"));
			int total = (Integer)props.get("prodPrice") + (Integer)props.get("prodAdmin");
			hist.setTotal(total);
			hist.setHistorian("id.co.hike.gateway.adapter.fsp.status.KomodoBillerStatus");
			hist.setFsp(fsp);
			hist.setOrganization(org);
			if(auth.containsKey("refund") && auth.get("refund")!=null)
			{
				try {
					hist.setMessageSend(mapper.writeValueAsString(auth.get("refund")));
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			obj.put("history", hist);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> unknownService(String service) {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message", "Unknown service requested " + service);
		return obj;
	}

	@Override
	public String getTimestamp() {
		// TODO Auto-generated method stub
		return timestamp;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return data.get("phoneNumber");
	}

	@Override
	public String getService() {
		// TODO Auto-generated method stub
		return this.service;
	}

	@Override
	public String getFSP() {
		// TODO Auto-generated method stub
		return "pg-nicepay";
	}

}
