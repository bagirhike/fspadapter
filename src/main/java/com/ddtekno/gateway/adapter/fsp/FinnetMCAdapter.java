package com.ddtekno.gateway.adapter.fsp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ThreadLocalRandom;

import com.ddtekno.gateway.adapter.model.Gateway;
import com.ddtekno.gateway.adapter.model.History;
import com.ddtekno.gateway.util.HMAC;
import com.ddtekno.gateway.util.HexStringConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FinnetMCAdapter implements Adapter {

	private Map<String, Object> obj;
	private Map<String, String> headers;
	private Map<String, String> data;

	private String fsp, timestamp, service, organization;
	
	private final ObjectMapper mapper = new ObjectMapper();

	@Override
	public Map<String, Object> init(String id, String orgId, String url, String service, Map<String, Object> auth) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		timestamp = sdf.format(new Date(System.currentTimeMillis()));
		this.service = service;
		this.organization = orgId;
		this.fsp = id;
		switch (service) {
		case "getToken":
			return getToken(id, url, service, auth);
		case "reqActivation":
			return reqActivation(id, url, service, auth);
		case "reqConfirmation":
			return reqConfirmation(id, url, service, auth);
		case "getBalance":
			return getBalance(id, url, service, auth);
		case "transaction":
			return transaction(id, url, service, auth);
		case "transferP2P":
			return transferP2P(id, url, service, auth);
		case "reversal":
			return reversal(id, url, service, auth);
		case "widgetTopUp":
			return widgetTopup(id, url, service, auth);
		case "mutasiBalance":
			return mutasiBalance(id, url, service, auth);
		case "getMasterAccount":
			return getMasterAccount(id, url, service, auth);
		case "widgetProfile":
			return widgetProfile(id, url, service, auth);
		case "widgetBillPayment":
			return widgetBillPayment(id, url, service, auth);
		case "getHist":
			return getHist(id, url, service, auth);
		case "unpair":
			return unpair(id, url, service, auth);
		default:
			return unknownService(service);
		}
	}

	private Map<String, Object> getHist(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String partnerKey = (String) auth.get("partnerKey");
		String phoneNumber = (String) auth.get("chanId");
		String tokenID = (String) auth.get("token");

		Date now = new Date(System.currentTimeMillis());
		Date last = new Date(System.currentTimeMillis() - (98 * 3600000));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));

		if (partnerKey != null && tokenID != null) {

			data.put("phoneNumber", phoneNumber);
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("tokenID", tokenID);
			data.put("transNumber", id);
			String key = phoneNumber + timestamp + service + tokenID + id;
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			data.put("endDate", sdf.format(now));
			data.put("startDate", sdf.format(last));
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> widgetBillPayment(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String partnerKey = (String) auth.get("partnerKey");
		String phoneNumber = (String) auth.get("chanId");
		String tokenID = (String) auth.get("token");
		if (partnerKey != null && tokenID != null) {

			data.put("phoneNumber", phoneNumber);
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("tokenID", tokenID);
			data.put("transNumber", id);
			String key = phoneNumber + timestamp + service + tokenID + id;
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> unpair(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String partnerKey = (String) auth.get("partnerKey");
		String phoneNumber = (String) auth.get("chanId");
		String tokenID = (String) auth.get("token");
		if (partnerKey != null && tokenID != null) {
			data.put("phoneNumber", phoneNumber);
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("tokenID", tokenID);
			data.put("transNumber", id);
			String key = phoneNumber + timestamp + service + tokenID + id;
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> transferP2P(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String partnerKey = (String) auth.get("partnerKey");
		String phoneNumber = (String) auth.get("chanId");
		String tokenID = (String) auth.get("token");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (partnerKey != null && tokenID != null && props != null && props.get("transAmount") != null
				&& props.get("transType") != null && props.get("transDesc") != null) {
			data.put("phoneNumber", phoneNumber);
			data.put("phoneNumberDest", props.get("phoneNumberDest"));
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("tokenID", tokenID);
			data.put("transAmount", props.get("transAmount"));
			data.put("transType", props.get("transType"));
			data.put("transDesc", props.get("transDesc"));
			data.put("transNumber", id);
			String key = phoneNumber + props.get("phoneNumberDest") + timestamp + service + tokenID
					+ props.get("transAmount") + props.get("transDesc") + id + props.get("transType");
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "Failed on Request, One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> getMasterAccount(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String partnerKey = (String) auth.get("partnerKey");
		String tokenID = (String) auth.get("token");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (partnerKey != null && tokenID != null && props != null && props.get("transType") != null) {
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("tokenID", tokenID);
			data.put("transType", props.get("transType"));
			data.put("transNumber", id);
			String key = timestamp + service + tokenID + id + props.get("transType");
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "Failed on Request, One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> reversal(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String partnerKey = (String) auth.get("partnerKey");
		String phoneNumber = (String) auth.get("chanId");
		String tokenID = (String) auth.get("token");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (partnerKey != null && tokenID != null && props != null
				&& props.get("appCode") != null) {
			data.put("appCode", props.get("appCode"));
			data.put("phoneNumber", phoneNumber);
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("tokenID", tokenID);
			data.put("transNumber", id);
			String key = props.get("appCode") + phoneNumber + timestamp + service + tokenID + id;
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "Failed on Request, One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> widgetProfile(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String partnerKey = (String) auth.get("partnerKey");
		String phoneNumber = (String) auth.get("chanId");
		String tokenID = (String) auth.get("token");
		if (partnerKey != null && tokenID != null) {
			data.put("phoneNumber", phoneNumber);
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("tokenID", tokenID);
			data.put("transNumber", id);
			String key = phoneNumber + timestamp + service + tokenID + id;
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> mutasiBalance(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String partnerKey = (String) auth.get("partnerKey");
		String phoneNumber = (String) auth.get("chanId");
		String tokenID = (String) auth.get("token");
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (partnerKey != null && tokenID != null && props != null && props.get("transAmount") != null
				&& props.get("transType") != null && props.get("transDesc") != null) {
			data.put("phoneNumber", phoneNumber);
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("tokenID", tokenID);
			data.put("transAmount", props.get("transAmount"));
			data.put("transType", props.get("transType"));
			data.put("transDesc", props.get("transDesc"));
			data.put("transNumber", id);
			String key = phoneNumber + timestamp + service + tokenID + props.get("transAmount") + props.get("transDesc")
					+ id + props.get("transType");
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "Failed on Request, One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> transaction(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("parser", "json");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		History hist = new History();
		String partnerKey = (String) auth.get("partnerKey");
		String phoneNumber = (String) auth.get("chanId");
		String tokenID = (String) auth.get("token");
		Map<String, Object> props = (Map<String, Object>) auth.get("props");
		if (partnerKey != null && tokenID != null && props != null && props.get("prodDesc") != null
				&& props.get("prodId") != null) {
			data.put("phoneNumber", phoneNumber);
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("tokenID", tokenID);
			int total = (Integer) props.get("prodAdmin") + (Integer) props.get("prodPrice");
			data.put("transAmount", String.valueOf(total));
			data.put("transType", props.get("prodType").toString());
			data.put("transDesc", props.get("prodDesc").toString());
			data.put("transNumber", id);
			String key = phoneNumber + timestamp + service + tokenID + String.valueOf(total) + props.get("prodDesc")
					+ id + props.get("prodType");
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			obj.put("data", data);
			
			hist.setReffNo(id);
			hist.setProduct(props.get("prodName").toString());
			hist.setDesc(props.get("prodDesc").toString());
			hist.setTimestamp(timestamp);
			hist.setChannelId(phoneNumber);
			hist.setPrice((Integer) props.get("prodPrice"));
			hist.setAdmin((Integer) props.get("prodAdmin"));
			hist.setTotal(total);
			hist.setSerialNumber(props.get("prodId").toString());
			hist.setFsp(fsp);
			hist.setOrganization(organization);
			hist.setHistorian("id.co.hike.gateway.adapter.fsp.status.FinnetMCStatus");
			
			if (props.containsKey("prodFSP")) {
				Gateway q = new Gateway();
				Gateway r = new Gateway();
				
				r.setId(phoneNumber);
				r.setFsp(fsp);
				r.setService("reversal");
				r.setToken(signature);
				r.setProps(new HashMap<String, Object>());
				
				Map<String, Object> p = new HashMap<String, Object>();
				String[] pf = props.get("prodFSP").toString().split(";");
				q.setId(phoneNumber);
				q.setFsp(pf[0]);
				q.setRefund(r);
				q.setService(pf[1]);
				p.put("prodId", props.get("prodId").toString());
				p.put(pf[2], props.get("prodDesc").toString());
				q.setProps(p);
				try {
					hist.setMessageSend(mapper.writeValueAsString(q));
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			obj.put("history", hist);

		} else {
			obj.put("message", "Failed on Request, One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> widgetTopup(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String partnerKey = (String) auth.get("partnerKey");
		String phoneNumber = (String) auth.get("chanId");
		String tokenID = (String) auth.get("token");
		if (partnerKey != null && tokenID != null) {
			data.put("phoneNumber", phoneNumber);
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("tokenID", tokenID);
			data.put("transNumber", id);
			String key = phoneNumber + timestamp + service + tokenID + id;
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> getBalance(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String partnerKey = (String) auth.get("partnerKey");
		String phoneNumber = (String) auth.get("chanId");
		String tokenID = (String) auth.get("token");
		if (partnerKey != null && tokenID != null) {
			data.put("phoneNumber", phoneNumber);
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("tokenID", tokenID);
			data.put("transNumber", id);
			String key = phoneNumber + timestamp + service + tokenID + id;
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> reqConfirmation(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String partnerKey = (String) auth.get("partnerKey");
		String phoneNumber = (String) auth.get("chanId");
		String tokenID = (String) auth.get("token");
		String custName = "mumu";
		Map<String, String> props = (Map<String, String>) auth.get("props");
		if (partnerKey != null && tokenID != null && props != null && props.get("custStatusCode") != null
				&& props.get("otp") != null) {
			data.put("custName", custName);
			data.put("custStatusCode", props.get("custStatusCode"));
			data.put("otp", props.get("otp"));
			data.put("phoneNumber", phoneNumber);
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("tokenID", tokenID);
			data.put("transNumber", id);
			String key = custName + props.get("custStatusCode") + props.get("otp") + phoneNumber + timestamp + service
					+ tokenID + id;
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "Failed on Request, One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> reqActivation(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String partnerKey = (String) auth.get("partnerKey");
		String phoneNumber = (String) auth.get("chanId");
		String tokenID = (String) auth.get("token");
		if (partnerKey != null && tokenID != null) {
			data.put("phoneNumber", phoneNumber);
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("tokenID", tokenID);
			data.put("transNumber", id);
			String key = phoneNumber + timestamp + service + tokenID + id;
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> unknownService(String service) {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message", "Unknown service requested " + service);
		return obj;
	}

	private Map<String, Object> getToken(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, String>();
		url = url + "?authMethod=Basic&authUsername=" + auth.get("username") + "&authPassword=" + auth.get("password");
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		obj.put("headers", headers);

		String partnerKey = (String) auth.get("partnerKey");
		if (partnerKey != null) {
			data.put("reqDtime", timestamp);
			data.put("requestType", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("transNumber", id);
			String key = timestamp + service + id;
			String secret = HexStringConverter.str2Hex(partnerKey).toUpperCase();
			String signature = HMAC.hmacDigest(key, secret, "HmacSHA256").toUpperCase();
			data.put("signature", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		// TODO Auto-generated method stub
		return obj;
	}

	@Override
	public String getTimestamp() {
		// TODO Auto-generated method stub
		return timestamp;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return data.get("phoneNumber");
	}

	@Override
	public String getService() {
		// TODO Auto-generated method stub
		return this.service;
	}

	@Override
	public String getFSP() {
		// TODO Auto-generated method stub
		return fsp;
	}

}
