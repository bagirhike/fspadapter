package com.ddtekno.gateway.adapter.fsp;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ThreadLocalRandom;

import com.ddtekno.gateway.adapter.model.History;
import com.ddtekno.gateway.util.HMAC;
import com.ddtekno.gateway.util.HexStringConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class POSBillerAdapterDev implements Adapter {

	private Map<String, Object> obj;
	private Map<String, String> headers;
	private Map<String, Object> data;

	private String timestamp, service, date, phoneNumber;
	
	private final ObjectMapper mapper = new ObjectMapper();

	@Override
	public Map<String, Object> init(String id, String orgId, String url, String service, Map<String, Object> auth) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		timestamp = sdf.format(new Date(System.currentTimeMillis()));
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		date = sdf.format(new Date(System.currentTimeMillis()));
		this.service = service;
		switch (service) {
		case "inquiry":
			return inquiry(id, url, service, auth);
		case "payment":
			return payment(id, url, service, auth);
		case "purchase":
			return purchase(id, url, service, auth);
		case "advice":
			return advice(id, url, service, auth);
		default:
			return unknownService(service);
		}
	}
	
	private Map<String, Object> advice(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, Object>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		headers.put("origin", "gateway.hike.id");
		headers.put("agent", "FinSBUGiroMPospay-B2B");
		String clientAuth = auth.get("client_id").toString()+":"+auth.get("client_secret").toString();
		headers.put("auth", Base64.getEncoder().encodeToString(clientAuth.getBytes()));
		
		obj.put("headers", headers);

		String institutionCode = (String) auth.get("institution_code");
		String hikeAccount = (String) auth.get("hike_account");
		String passwordAPI = (String) auth.get("password_api");
		String keyAPI = (String) auth.get("key_api");
		String accountType = (String) auth.get("account_type");
		phoneNumber = getPhoneNumber((String) auth.get("chanId"));
		String signature = (String) auth.get("signature");
		
		Map<String, String> props = (Map<String, String>) auth.get("props");
		
		if (phoneNumber != null && props != null && props.containsKey("prodId") 
				&& props.containsKey("billAccount") && props.containsKey("code")) {
			
			data.put("account", hikeAccount);
			data.put("accountType", accountType);
			data.put("retrieval", timestamp);
			data.put("type", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("institutionCode", institutionCode);
			data.put("trxId", id);
			data.put("product", props.get("prodId"));
			data.put("billNumber", props.get("billAccount"));
			data.put("paymentCode", props.get("code"));
			String key = service + phoneNumber + institutionCode + props.get("prodId") + props.get("billAccount") + id + passwordAPI + date + keyAPI;
			if (signature == null) {
				try {
					signature = encrypt(key);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			data.put("sign", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}
	
	private Map<String, Object> purchase(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, Object>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("contentType", "application/json");
		headers.put("origin", "gateway.hike.id");
		headers.put("agent", "FinSBUGiroMPospay-B2B");
		String clientAuth = auth.get("client_id").toString()+":"+auth.get("client_secret").toString();
		headers.put("auth", Base64.getEncoder().encodeToString(clientAuth.getBytes()));
		
		obj.put("headers", headers);

		String institutionCode = (String) auth.get("institution_code");
		String hikeAccount = (String) auth.get("hike_account");
		String passwordAPI = (String) auth.get("password_api");
		String keyAPI = (String) auth.get("key_api");
		String accountType = (String) auth.get("account_type");
		phoneNumber = getPhoneNumber((String) auth.get("chanId"));
		String signature = (String) auth.get("signature");
		
		Map<String, String> props = (Map<String, String>) auth.get("props");
		
		if (phoneNumber != null && props != null && props.containsKey("prodId") && props.containsKey("billAccount")) {
			
			data.put("account", hikeAccount);
			data.put("accountType", accountType);
			data.put("retrieval", timestamp);
			data.put("type", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("institutionCode", institutionCode);
			data.put("trxId", id);
			data.put("product", props.get("prodId"));
			data.put("billNumber", props.get("billAccount"));
			String key = service + phoneNumber + institutionCode + props.get("prodId") + props.get("billAccount") + id + passwordAPI + date + keyAPI;
			if (signature == null) {
				try {
					signature = encrypt(key);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			data.put("sign", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}

	private Map<String, Object> payment(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, Object>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("parser", "json");
		headers.put("contentType", "application/json");
		headers.put("origin", "gateway.hike.id");
		headers.put("agent", "FinSBUGiroMPospay-B2B");
		String clientAuth = auth.get("client_id").toString()+":"+auth.get("client_secret").toString();
		headers.put("auth", Base64.getEncoder().encodeToString(clientAuth.getBytes()));
		
		obj.put("headers", headers);

		History hist = new History();
		String institutionCode = (String) auth.get("institution_code");
		String hikeAccount = (String) auth.get("hike_account");
		String passwordAPI = (String) auth.get("password_api");
		String keyAPI = (String) auth.get("key_api");
		String accountType = (String) auth.get("account_type");
		phoneNumber = getPhoneNumber((String) auth.get("chanId"));
		String signature = (String) auth.get("signature");
		int nominal = 0, total = 0;
		Map<String, Object> props = (Map<String, Object>) auth.get("props");
		
		if (phoneNumber != null && props != null && props.containsKey("prodId") 
				&& props.containsKey("billAccount") && props.containsKey("code")) {
			
			data.put("account", hikeAccount);
			data.put("accountType", accountType);
			data.put("retrieval", timestamp);
			data.put("type", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("institutionCode", institutionCode);
			data.put("trxId", id);
			data.put("product", props.get("prodId"));
			data.put("billNumber", props.get("billAccount"));
			data.put("paymentCode", props.get("code"));
			String key = service + phoneNumber + institutionCode + props.get("prodId") + props.get("billAccount") + id + passwordAPI + date + keyAPI;
			if (signature == null) {
				try {
					signature = encrypt(key);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			data.put("sign", signature);
			obj.put("data", data);
			
			if(props.containsKey("nominal"))
				nominal = Integer.parseInt(props.get("nominal").toString());
			
			nominal = nominal + ((Integer) props.get("prodPrice"));
			
			total = nominal + (Integer)props.get("prodAdmin");
			
			hist.setReffNo(id);
			hist.setProduct(props.get("prodName").toString());
			hist.setDesc((String)props.get("prodDesc"));
			hist.setTimestamp(timestamp);
			hist.setChannelId(phoneNumber);
			hist.setPrice(nominal);
			hist.setAdmin((Integer)props.get("prodAdmin"));
			hist.setTotal(total);
			hist.setSerialNumber(props.get("prodId").toString());
			hist.setHistorian("id.co.hike.gateway.adapter.fsp.status.POSBillerStatus");
			
			if(auth.containsKey("refund") && auth.get("refund")!=null)
			{
				try {
					hist.setMessageSend(mapper.writeValueAsString(auth.get("refund")));
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			obj.put("history", hist);
			
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}
	
	private Map<String, Object> inquiry(String id, String url, String service, Map<String, Object> auth) {
		obj = new HashMap<String, Object>();
		headers = new HashMap<String, String>();
		data = new HashMap<String, Object>();
		obj.put("url", url);
		obj.put("method", "POST");
		headers.put("parser", "json");
		headers.put("contentType", "application/json");
		headers.put("origin", "gateway.hike.id");
		headers.put("agent", "FinSBUGiroMPospay-B2B");
		String clientAuth = auth.get("client_id").toString()+":"+auth.get("client_secret").toString();
		headers.put("auth", Base64.getEncoder().encodeToString(clientAuth.getBytes()));
		
		obj.put("headers", headers);

		History hist = new History();
		String institutionCode = (String) auth.get("institution_code");
		String hikeAccount = (String) auth.get("hike_account");
		String passwordAPI = (String) auth.get("password_api");
		String keyAPI = (String) auth.get("key_api");
		String accountType = (String) auth.get("account_type");
		phoneNumber = getPhoneNumber((String) auth.get("chanId"));
		String signature = (String) auth.get("signature");
		int nominal = 0, total = 0;
		
		Map<String, Object> props = (Map<String, Object>) auth.get("props");
		
		if (phoneNumber != null && props != null && props.containsKey("prodId") && props.containsKey("billAccount")) {

			data.put("account", hikeAccount);
			data.put("accountType", accountType);
			data.put("retrieval", timestamp);
			data.put("type", service);
			id = "" + ThreadLocalRandom.current().nextInt(1000000);
			data.put("institutionCode", institutionCode);
			data.put("trxId", id);
			data.put("product", props.get("prodId"));
			data.put("billNumber", props.get("billAccount"));
			String key = service + phoneNumber + institutionCode + props.get("prodId") + props.get("billAccount") + id + passwordAPI + date + keyAPI;
			if (signature == null) {
				try {
					signature = encrypt(key);
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			hist.setPrice((Integer)props.get("prodPrice"));
			hist.setAdmin((Integer)props.get("prodAdmin"));
			hist.setHistorian("id.co.hike.gateway.adapter.fsp.status.POSBillerStatus");
			obj.put("history", hist);
			
			data.put("sign", signature);
			obj.put("data", data);
		} else {
			obj.put("message", "One of required data not found");
		}

		return obj;
	}


	private Map<String, Object> unknownService(String service) {
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("message", "Unknown service requested " + service);
		return obj;
	}

	private String encrypt(String val) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(val.getBytes(StandardCharsets.UTF_8));
		byte byteData[] = md.digest();
		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
	
	private String getPhoneNumber(String string) {
		return string.split("@")[0];
	}

	@Override
	public String getTimestamp() {
		// TODO Auto-generated method stub
		return timestamp;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return phoneNumber;
	}

	@Override
	public String getService() {
		// TODO Auto-generated method stub
		return this.service;
	}

	@Override
	public String getFSP() {
		// TODO Auto-generated method stub
		return "sof-giro-pos";
	}

}
