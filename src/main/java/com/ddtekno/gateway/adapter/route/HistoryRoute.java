package com.ddtekno.gateway.adapter.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import com.ddtekno.gateway.adapter.repo.HistoryPersistence;

@Component
public class HistoryRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		
		from("direct:startHistoryConfig").id("gethistoryconfig")
		.bean(HistoryPersistence.class, "history").end();
		
		from("direct:updateHistoryConfig").id("updatehistoryconfig")
		.bean(HistoryPersistence.class, "update").end();
	}
}