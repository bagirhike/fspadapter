package com.ddtekno.gateway.adapter.route;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;

import com.ddtekno.gateway.adapter.model.Query;
import com.ddtekno.gateway.adapter.processor.FSPProcessor;
import com.ddtekno.gateway.adapter.processor.QueryProcessor;
import com.ddtekno.gateway.adapter.processor.ResponseProcessor;
import com.ddtekno.gateway.adapter.repo.FSPPersistence;
import com.ddtekno.gateway.adapter.repo.HistoryPersistence;

@Component
public class FSPCallRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		JacksonDataFormat jsonDataFormat = new JacksonDataFormat(Query.class);

		from("direct:startCallFSPConfig").id("callfspconfig").marshal(jsonDataFormat)
		.process(new QueryProcessor())
		.bean(FSPPersistence.class, "call")
		.process(new FSPProcessor())
		.choice()
			.when().header("url")
				.toD("${header.url}")
			    .process(new ResponseProcessor())
				.bean(HistoryPersistence.class,"commit")
				.convertBodyTo(String.class)
			.otherwise()
				.convertBodyTo(String.class)
		.end();
	}
}