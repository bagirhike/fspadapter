package com.ddtekno.gateway.adapter.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;

import com.ddtekno.gateway.adapter.model.FSP;
import com.ddtekno.gateway.adapter.processor.FSPProcessor;
import com.ddtekno.gateway.adapter.repo.FSPPersistence;

@Component
public class FSPAddRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		JacksonDataFormat jsonDataFormat = new JacksonDataFormat(FSP.class);

		from("direct:startAddFSPConfig").id("addfspconfig").marshal(jsonDataFormat)
		.bean(FSPPersistence.class, "commit")
		/*.log("Test auth and service").process(new FSPProcessor("getToken"))
		.choice()
			.when().simple("${header.error} == null")
				.toD("${header.url}")
			.otherwise()
				.log("{body}")*/.end();
	}
}