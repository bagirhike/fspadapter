package com.ddtekno.gateway.adapter.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;

import com.ddtekno.gateway.adapter.model.Query;
import com.ddtekno.gateway.adapter.repo.FSPPersistence;

@Component
public class FSPFindRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		JacksonDataFormat jsonDataFormat = new JacksonDataFormat(Query.class);

		from("direct:startGetFSPConfig").id("getfspconfig").marshal(jsonDataFormat)
		.bean(FSPPersistence.class, "get").end();
	}
}