package com.ddtekno.gateway.adapter.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.stereotype.Component;

import com.ddtekno.gateway.adapter.model.Query;
import com.ddtekno.gateway.adapter.processor.FSPProcessor;
import com.ddtekno.gateway.adapter.processor.QueryProcessor;
import com.ddtekno.gateway.adapter.processor.ResponseProcessor;
import com.ddtekno.gateway.adapter.repo.FSPPersistence;
import com.ddtekno.gateway.adapter.repo.HistoryPersistence;

@Component
public class POSDevRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		from("direct:posDev").id("posdev")
		.to("https://wlgiro.posindonesia.co.id:7443/hike/").process(new ResponseProcessor())
		.bean(HistoryPersistence.class,"commit")
		.convertBodyTo(String.class).end();
	}
}