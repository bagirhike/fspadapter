package com.ddtekno.gateway.adapter.model;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

public class History {
	
	/*
	 * data yg dibutuhkan untuk history transaksi mumu = 1. status transaksi
	 * (sukses/gagal/pending) 2. tgl transaksi 3. produk transaksi (Donasi dll) 4.
	 * id produk 5. track id/nomor referensi 6. jumlah bayar 7. biaya admin 8. total
	 * bayar 9. serial number 10. id pelanggan
	 */
	
	@Id
	private String reffNo;
	
	private String status;
	private String timestamp;
	private String product;
	private String desc;
	private int price;
	private int admin;
	private int total;
	private String serialNumber;
	private String channelId;
	private Map<String,Object> messageResponse;
	private String messageSend;
	private String organization;
	private String info;
	private String fsp;
	private Map<String,Object> settlement;
	
	@Transient
	private String historian;
	
	@Transient
	private String action;
	
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getReffNo() {
		return reffNo;
	}
	public void setReffNo(String reffNo) {
		this.reffNo = reffNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
	public int getAdmin() {
		return admin;
	}
	public void setAdmin(int admin) {
		this.admin = admin;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getHistorian() {
		return historian;
	}
	public void setHistorian(String historian) {
		this.historian = historian;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Map<String,Object> getMessageResponse() {
		return messageResponse;
	}
	public void setMessageResponse(Map<String,Object> messageResponse) {
		this.messageResponse = messageResponse;
	}
	public String getMessageSend() {
		return messageSend;
	}
	public void setMessageSend(String messageSend) {
		this.messageSend = messageSend;
	}
	public String getOrganization() {
		return organization;
	}
	public String getFsp() {
		return fsp;
	}
	public void setFsp(String fsp) {
		this.fsp = fsp;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public Map<String,Object> getSettlement() {
		return settlement;
	}
	public void setSettlement(Map<String, Object> map) {
		this.settlement = map;
	}

}
