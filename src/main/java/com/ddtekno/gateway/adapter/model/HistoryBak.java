package com.ddtekno.gateway.adapter.model;

import java.io.Serializable;
import java.util.Map;

import org.springframework.data.annotation.Id;

public class HistoryBak {
	
	@Id
	private Key id;

	private String service;
	private String code;
	private Map<String,Object> message;
	
	public Key getId() {
		return id;
	}
	public void setId(Key id) {
		this.id = id;
	}
	
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Map<String,Object> getMessage() {
		return message;
	}
	public void setMessage(Map<String,Object> message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return "History [id=" + id + ", service=" + service + ", code=" + code + ", message=" + message + "]";
	}

	public static class Key implements Serializable {
		@Override
		public String toString() {
			return "Key [channelId=" + channelId + ", timestamp=" + timestamp + ", fspId=" + fspId + "]";
		}
		private String channelId;
		private long timestamp;
		private String fspId;
		
		public Key(String channelId, long timestamp, String fspId) {
			this.channelId = channelId;
			this.timestamp = timestamp;
			this.fspId = fspId;
		}
		
		public String getChannelId() {
			return channelId;
		}
		public void setChannelId(String channelId) {
			this.channelId = channelId;
		}
		public long getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(long timestamp) {
			this.timestamp = timestamp;
		}
		public String getFspId() {
			return fspId;
		}
		public void setFspId(String fspId) {
			this.fspId = fspId;
		}
	}

}
