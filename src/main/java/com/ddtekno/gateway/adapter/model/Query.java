package com.ddtekno.gateway.adapter.model;

import java.util.Map;

public class Query {
	
	private String id;
	private String name;
	private String type;
	private String service;
	private String signature;
	private String orgId;
	
	private Map<String, Object> auth;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Map<String, Object> getAuth() {
		return auth;
	}
	public void setAuth(Map<String, Object> auth) {
		this.auth = auth;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	@Override
	public String toString() {
		return "Query [id=" + id + ", name=" + name + ", type=" + type + ", service=" + service + ", auth=" + auth
				+ "]";
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

}
