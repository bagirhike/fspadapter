package com.ddtekno.gateway.adapter.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.ddtekno.gateway.adapter.model.Query;
import com.fasterxml.jackson.databind.ObjectMapper;

public class QueryProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		ObjectMapper obj = new ObjectMapper();
		String body = exchange.getIn().getBody(String.class);
		
		Query conf = obj.readValue(body, Query.class);
		exchange.getOut().setHeader("service", conf.getService());
		exchange.getOut().setHeader("org", conf.getOrgId());
		
		exchange.getOut().setBody(body);
	}

}
