package com.ddtekno.gateway.adapter.processor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.ddtekno.gateway.adapter.fsp.status.TransactionStatus;
import com.ddtekno.gateway.adapter.model.History;
import com.ddtekno.gateway.adapter.model.Query;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ResponseProcessor implements Processor {

	private ObjectMapper mapper = new ObjectMapper();

	@Override
	public void process(Exchange exchange) throws Exception {
		String body = exchange.getIn().getBody(String.class);

		// For Finnet emony Bug
		if (body.startsWith("\\"))
			body = body.replace("\\", "");

		if (exchange.getIn().getHeader("history") != null) {
			History resp = exchange.getIn().getHeader("history", History.class);
			TransactionStatus status = (TransactionStatus) Class.forName(resp.getHistorian()).newInstance();
			String stat = null;

			if (exchange.getIn().getHeader("parser").equals("html")) {
				Document doc = Jsoup.parse(body);
				Map<String, Object> map = new HashMap<String, Object>();

				for (Element i : doc.select("input")) {
					map.put(i.attr("name"), i.attr("value"));
				}

				stat = status.read(map);
				resp.setStatus(stat);
				resp.setAction(status.getAction());

				resp.setMessageResponse(map);

			} else {
				Map<String, Object> mapResp = mapper.readValue(body, Map.class);
				stat = status.read(mapResp);
				resp.setStatus(stat);
				resp.setAction(status.getAction());

				Map<String, Object> mapBody = status.transformResponse(mapResp, resp);
				resp.setMessageResponse(mapBody);
				resp = status.transformHistory(mapBody, resp);
			}

			if (resp.getStatus().equals("Sukses"))
				resp.setSettlement(status.settle(resp));
				

			exchange.getOut().setBody(resp, History.class);

		} else {
			History resp = new History();

			// Patch POS Dev
			String reqBody = exchange.getIn().getHeader("body", String.class);
			if (reqBody != null)
				resp.setMessageSend(reqBody);

			resp.setMessageResponse(mapper.readValue(body, Map.class));
			exchange.getOut().setBody(resp, History.class);
		}
	}

}
