package com.ddtekno.gateway.adapter.processor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.common.message.CxfConstants;

import com.ddtekno.gateway.adapter.fsp.Adapter;
import com.ddtekno.gateway.adapter.model.FSP;
import com.ddtekno.gateway.adapter.model.Gateway;
import com.ddtekno.gateway.adapter.model.Service;
import com.ddtekno.soap.e2pay.WsbgRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FSPProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		ObjectMapper obj = new ObjectMapper();
		String service = exchange.getIn().getHeader("service", String.class);
		String org = exchange.getIn().getHeader("org", String.class);
		Gateway refund = exchange.getIn().getHeader("refund", Gateway.class);
		FSP conf = obj.readValue(exchange.getIn().getBody(String.class), FSP.class);
		if (conf.getMessage() != null) {
			exchange.getMessage().setBody(conf.getMessage(), String.class);
			return;
		}

		Map<String, Object> auth = conf.getAuth();
		String id = conf.getId();
		String url = conf.getUrl();

		for (Service s : conf.getServices()) {
			if (s.getPath() != null && s.getName().equals(service)) {
				url = url + s.getPath();
			}
		}

		Adapter adapter = (Adapter) Class.forName(conf.getAdapter()).newInstance();

		Map<String, Object> data = adapter.init(id, org, url, service, auth);
		Map<String, Object> headers = (Map<String, Object>) data.get("headers");

		exchange.getMessage().setHeader(Exchange.HTTP_METHOD, data.get("method"));
		exchange.getMessage().setHeader(Exchange.CONTENT_TYPE, headers.get("contentType"));
		exchange.getMessage().setHeader("url", data.get("url"));

		if (headers.containsKey("parser"))
			exchange.getMessage().setHeader("parser", headers.get("parser"));

		if (headers.containsKey("query"))
			exchange.getMessage().setHeader(Exchange.HTTP_QUERY, headers.get("query"));

		if (headers.containsKey("auth"))
			exchange.getMessage().setHeader("Authorization", headers.get("auth"));

		if (headers.containsKey("origin"))
			exchange.getMessage().setHeader("Origin", headers.get("origin"));

		if (headers.containsKey("agent"))
			exchange.getMessage().setHeader("User-Agent", headers.get("agent"));

		if (headers.containsKey("type")) {

			if (headers.get("type").equals("xform")) {
				exchange.getMessage().setBody(data.get("data"), String.class);
			}

			if (headers.get("type").equals("soap")) {
				exchange.getMessage().setHeader("authorization", headers.get("signature"));
				exchange.getMessage().setHeader("date", headers.get("timeauth"));
				exchange.getMessage().setHeader("service", service);
				exchange.getMessage().setBody(data.get("data"), WsbgRequest.class);
			}

		}

		if (headers.containsKey("CIMB-ApiKey")) {

			exchange.getMessage().setHeader("Accept", headers.get("contentType"));
			exchange.getMessage().setHeader("CIMB-ApiKey", headers.get("CIMB-ApiKey"));
			exchange.getMessage().setHeader("CIMB-Service", headers.get("CIMB-Service"));
			exchange.getMessage().setHeader("CIMB-Timestamp", headers.get("CIMB-Timestamp"));
			exchange.getMessage().setHeader("CIMB-Signature", headers.get("CIMB-Signature"));

		}

		if (data.containsKey("message")) {
			exchange.getMessage().setBody(data.get("message"), String.class);
		} else if (data.containsKey("history")) {
			exchange.getMessage().setHeader("history", data.get("history"));
			exchange.getMessage().setBody(obj.writeValueAsString(data.get("data")), String.class);
		} else {

			exchange.getMessage().setBody(obj.writeValueAsString(data.get("data")), String.class);

		}
	}

}
