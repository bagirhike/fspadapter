package com.ddtekno.gateway.adapter.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.ddtekno.gateway.adapter.model.History;

public interface HistoryRepository extends MongoRepository<History, String> {

	@Query(value = "{'channelId': ?0}")
	public List<History> findHistory(String channelId);

}
