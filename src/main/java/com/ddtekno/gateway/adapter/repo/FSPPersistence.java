package com.ddtekno.gateway.adapter.repo;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.ddtekno.gateway.adapter.model.FSP;
import com.ddtekno.gateway.adapter.model.Query;
import com.ddtekno.gateway.adapter.model.Service;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FSPPersistence {

	@Autowired
	private FSPRepository repo;

	private ObjectMapper obj;
	
	private String message;
	
	private FSP f;

	public String commit(String json) throws JsonParseException, JsonMappingException, IOException {

		obj = new ObjectMapper();

		FSP conf = obj.readValue(json, FSP.class);

		repo.save(conf);

		return json;
	}

	public String get(String json) throws JsonParseException, JsonMappingException, IOException {

		obj = new ObjectMapper();

		Query q = obj.readValue(json, Query.class);
		
		if(repo.existsById(q.getId())) {
			Optional<FSP> fsp = repo.findById(q.getId());
			message = obj.writeValueAsString(fsp.get());
		}else {
			f = new FSP();
			f.setMessage("FSP by ID "+q.getId()+" Not found");
			message = obj.writeValueAsString(f);
		}

		return message;
	}
	
	public String call(String json) throws JsonParseException, JsonMappingException, IOException {

		obj = new ObjectMapper();

		Query q = obj.readValue(json, Query.class);
		
		if(repo.existsById(q.getId())) {
			Optional<FSP> fsp = repo.findById(q.getId());
			f = fsp.get();
			System.out.println(obj.writeValueAsString(f));
			for(Service s : f.getServices()) {
				if(s.getName().equals(q.getService())){
					Map<String,Object> auth = q.getAuth();
					
					if(q.getSignature()!=null)
						auth.put("signature", q.getSignature());
					
					f.setAuth(auth);
					message = obj.writeValueAsString(f);
				}
			}
			
			if(message==null) {
				f = new FSP();
				f.setMessage("Service Not found");
				message = obj.writeValueAsString(f);
			}
		}else {
			f = new FSP();
			f.setMessage("FSP Not found");
			message = obj.writeValueAsString(f);
		}
		
        
		return message;
	}

}
