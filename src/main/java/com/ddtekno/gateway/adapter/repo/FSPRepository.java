package com.ddtekno.gateway.adapter.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ddtekno.gateway.adapter.model.FSP;

public interface FSPRepository extends MongoRepository<FSP, String> {

	public List<FSP> findByName(String name);

}
