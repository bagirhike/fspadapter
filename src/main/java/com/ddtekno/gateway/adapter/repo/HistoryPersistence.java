package com.ddtekno.gateway.adapter.repo;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.ddtekno.gateway.adapter.fsp.status.TransactionStatus;
import com.ddtekno.gateway.adapter.model.History;
import com.ddtekno.gateway.adapter.model.HistoryBak;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HistoryPersistence {

	@Autowired
	private HistoryRepository repo;

	List<History> histories;

	final ObjectMapper obj = new ObjectMapper();

	public String commit(History conf) throws Exception {

		if (conf.getReffNo() != null)
			repo.save(conf);

		if (conf.getAction() != null && conf.getMessageSend()!=null) {
			Map<String, String> m1 = obj.readValue(conf.getMessageSend(), Map.class);
			
			m1.put("action", conf.getAction());

			return obj.writeValueAsString(m1);
		}

		return obj.writeValueAsString(conf.getMessageResponse());
	}

	public String history(String channelId) throws Exception {

		histories = repo.findHistory(channelId);

		return obj.writeValueAsString(histories);
	}
	
	public void update(History conf) {
		
		Optional<History> op = repo.findById(conf.getReffNo());
		
		if(op.isPresent()) {
			History h = op.get();
			h.setStatus(conf.getStatus());
			h.setMessageResponse(conf.getMessageResponse());
			repo.save(h);
		}
		
		
	}

}
