package com.ddtekno.gateway.adapter.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ddtekno.gateway.adapter.model.FSP;
import com.ddtekno.gateway.adapter.model.History;
import com.ddtekno.gateway.adapter.model.Query;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class FSPController {

	@Produce(uri = "direct:startAddFSPConfig")
	private ProducerTemplate addFSP;

	@Produce(uri = "direct:startGetFSPConfig")
	private ProducerTemplate getFSP;

	@Produce(uri = "direct:startCallFSPConfig")
	private ProducerTemplate callFSP;

	@Produce(uri = "direct:startHistoryConfig")
	private ProducerTemplate getHistory;
	
	@Produce(uri = "direct:updateHistoryConfig")
	private ProducerTemplate updateHistory;

	// POS Dev
	@Produce(uri = "direct:posDev")
	private ProducerTemplate posDev;

	@Produce(uri = "direct:posSignDev")
	private ProducerTemplate posSignDev;
	
	private final ObjectMapper mapper = new ObjectMapper();

	@RequestMapping(value = "/fsp/add", method = RequestMethod.POST)
	@ResponseBody
	public String addFSPConfiguration(@RequestBody FSP conf) throws InterruptedException, ExecutionException {

		Object msg = addFSP.requestBody(addFSP.getDefaultEndpoint(), conf);
		return msg.toString();
	}

	@RequestMapping(value = "/fsp/get", method = RequestMethod.GET)
	@ResponseBody
	public String findFSPbyID(@RequestParam String id) throws InterruptedException, ExecutionException {

		Query query = new Query();
		query.setId(id);

		Object msg = getFSP.requestBody(getFSP.getDefaultEndpoint(), query);
		return msg.toString();
	}

	@RequestMapping(value = "/fsp/call", method = RequestMethod.POST)
	@ResponseBody
	public String callFSP(@RequestBody Query query) throws InterruptedException, ExecutionException {
		if (query.getId() == null) {
			return "{\"message\":\"key id is mandatory\"}";
		} else {
			Object msg = callFSP.requestBody(callFSP.getDefaultEndpoint(), query);

			// Patch POS Dev
			try {
				JSONObject json = new JSONObject(msg.toString());
				if (json.has("action")) {
					String action = json.getString("action"); 
					if (action.equals("posdev")) 
						msg = posDev.requestBody(posDev.getDefaultEndpoint(), json.toString());
					
				}

			} catch (Exception err) {
			}

			return msg.toString();
		}
	}

	@RequestMapping(value = "/fsp/history", method = RequestMethod.GET)
	@ResponseBody
	public String history(@RequestParam String channelId) throws InterruptedException, ExecutionException {

		/*
		 * Date now = new Date(System.currentTimeMillis()); Date last = new
		 * Date(System.currentTimeMillis() - ((interval*3600)*1000));
		 * 
		 * SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		 * sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta")); String endTime =
		 * sdf.format(now); String startTime = sdf.format(last);
		 */

		Object msg = getHistory.requestBody(getHistory.getDefaultEndpoint(), channelId);
		return msg.toString();
	}
	
	@RequestMapping(value = "/fsp/update_history", method = RequestMethod.GET)
	@ResponseBody
	public String updateHistory(@RequestParam String id, @RequestParam String status, @RequestParam String message, @RequestParam String info) throws InterruptedException, ExecutionException {

		/*
		 * Date now = new Date(System.currentTimeMillis()); Date last = new
		 * Date(System.currentTimeMillis() - ((interval*3600)*1000));
		 * 
		 * SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		 * sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta")); String endTime =
		 * sdf.format(now); String startTime = sdf.format(last);
		 */
		
		History hist = new History();
		hist.setReffNo(id);
		hist.setStatus(status);
		try {
			hist.setMessageResponse(mapper.readValue(message,Map.class));
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		hist.setInfo(info);

		Object msg = updateHistory.requestBody(updateHistory.getDefaultEndpoint(), hist);
		
		return msg.toString();
	}

}
