package com.ddtekno.soap;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import org.apache.axis.Message;
import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.axis.message.SOAPEnvelope;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ddtekno.soap.finnet.FinnetRitnasServiceLocator;
import com.ddtekno.soap.finnet.FinnetRitnas_PortType;
import com.ddtekno.soap.finnet.InputCheck;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SOAPFinnetRitnasRequestProduction {

	private ObjectMapper mapper = new ObjectMapper();

	public String send(@Header("service") String service, @Body InputCheck input) throws ServiceException {
		FinnetRitnasServiceLocator serviceLocator = new FinnetRitnasServiceLocator();

		 // Use to get a proxy class for FinnetRitnas
	    String address = "http://172.31.253.133:8070/FINNET_WAY4GATE2/httpadapter/MTadapter2";
	    
	    java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }

	    FinnetRitnas_PortType stub = serviceLocator.getFinnetRitnas(endpoint);

		try {
			switch (service) {
			case "billpayment":
				return billpayment(stub, input);
			default:
				throw new IllegalArgumentException("Invalid service header " + service);
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new ServiceException("Invalid Json");
		}

	}

	private String billpayment(FinnetRitnas_PortType stub, InputCheck input)
			throws JsonProcessingException {
		SOAPEnvelope envelope = new SOAPEnvelope();
		Element e = null;
		Map<String, String> data = new HashMap<String, String>();

		try {
			envelope.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
			envelope.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			envelope.setAttribute("xmlns:soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
			envelope.setAttribute("xmlns:urn", "urn:ritnasXML");

			SOAPBodyElement operation = new SOAPBodyElement();
			operation.setName("urn:billpayment");
			operation.setAttribute("soapenv:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding");

			envelope.addBodyElement(operation);

			MessageElement req = new MessageElement();
			req.setName("inputCheck");
			req.setAttribute("xsi:type", "urn:inputCheck");
			req.setAttribute("xmlns:urn", "urn:routeDx2");
			operation.addChild(req);
			

			MessageElement userName = new MessageElement(new QName("userName"), input.getUserName());
			e = userName.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement signature = new MessageElement(new QName("signature"), input.getSignature());
			e = signature.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement merchantNumber = new MessageElement(new QName("merchantNumber"), input.getMerchantNumber());
			e = merchantNumber.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement merchantCode = new MessageElement(new QName("merchantCode"), input.getMerchantCode());
			e = merchantCode.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement terminal = new MessageElement(new QName("terminal"), input.getMerchantCode());
			e = terminal.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement branchCode = new MessageElement(new QName("branchCode"), input.getBranchCode());
			e = branchCode.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement locationCode = new MessageElement(new QName("locationCode"), input.getLocationCode());
			e = locationCode.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement productCode = new MessageElement(new QName("productCode"), input.getProductCode());
			e = productCode.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));
			
			MessageElement transactionType = new MessageElement(new QName("transactionType"), input.getTransactionType());
			e = transactionType.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));
			
			MessageElement billNumber = new MessageElement(new QName("billNumber"), input.getBillNumber());
			e = billNumber.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));
			
			MessageElement amount = new MessageElement(new QName("amount"), input.getAmount());
			e = amount.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));
			
			MessageElement traxId = new MessageElement(new QName("traxId"), input.getTraxId());
			e = traxId.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));
			
			MessageElement bit61 = new MessageElement(new QName("bit61"), input.getBit61());
			e = bit61.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));
			
			MessageElement timeStamp = new MessageElement(new QName("timeStamp"), input.getTimeStamp());
			e = timeStamp.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));
			
			MessageElement packetType = new MessageElement(new QName("packetType"), input.getPacketType());
			e = packetType.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

		} catch (SOAPException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		try {

			Message msg = new Message(envelope);

			org.apache.axis.message.SOAPEnvelope resp = stub.billPayment(msg);
			
			System.out.println(resp.getAsString());

			MessageElement body = resp.getFirstBody();
			
			MessageElement response = (MessageElement) body.getChildElements().next();
			
			Iterator elements = response.getChildElements();

			while(elements.hasNext()) {
				MessageElement m = (MessageElement) elements.next();
				data.put(m.getLocalName(), m.getValue());
			}

		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		return mapper.writeValueAsString(data);
	}

}
