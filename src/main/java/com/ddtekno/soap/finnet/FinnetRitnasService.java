/**
 * WsBillerGatewayProxyService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ddtekno.soap.finnet;

public interface FinnetRitnasService extends javax.xml.rpc.Service {
    public java.lang.String getFinnetRitnasAddress();

    public com.ddtekno.soap.finnet.FinnetRitnas_PortType getFinnetRitnas() throws javax.xml.rpc.ServiceException;

    public com.ddtekno.soap.finnet.FinnetRitnas_PortType getFinnetRitnas(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
