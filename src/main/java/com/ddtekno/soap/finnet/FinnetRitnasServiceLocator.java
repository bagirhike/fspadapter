/**
 * WsBillerGatewayProxyServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ddtekno.soap.finnet;

import java.net.URL;

import javax.xml.rpc.ServiceException;

public class FinnetRitnasServiceLocator extends org.apache.axis.client.Service implements com.ddtekno.soap.finnet.FinnetRitnasService {

    public FinnetRitnasServiceLocator() {
    }


    public FinnetRitnasServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public FinnetRitnasServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WsBillerGatewayProxy
    private java.lang.String FinnetRitnas_address = "http://172.31.253.134:8070/FINNET_WAY4GATE2/httpadapter/MTadapter2";


    // The WSDD service name defaults to the port name.
    private java.lang.String FinnetRitnasWSDDServiceName = "ritnasXML";

    public java.lang.String geFinnetRitnasWSDDServiceName() {
        return FinnetRitnasWSDDServiceName;
    }

    public void setFinnetRitnasWSDDServiceName(java.lang.String name) {
    	FinnetRitnasWSDDServiceName = name;
    }

    public void setWsBillerGatewayProxyEndpointAddress(java.lang.String address) {
    	FinnetRitnas_address = address;
    }


	@Override
	public String getFinnetRitnasAddress() {
		return FinnetRitnas_address;
	}


	@Override
	public FinnetRitnas_PortType getFinnetRitnas() throws ServiceException {
		java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(FinnetRitnas_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getFinnetRitnas(endpoint);
	}


	@Override
	public FinnetRitnas_PortType getFinnetRitnas(URL portAddress) throws ServiceException {
		try {
            com.ddtekno.soap.finnet.FinnetRitnasSoapBindingStub _stub = new com.ddtekno.soap.finnet.FinnetRitnasSoapBindingStub(portAddress, this);
            _stub.setPortName(geFinnetRitnasWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
	}

}
