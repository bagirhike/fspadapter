package com.ddtekno.soap;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import org.apache.axis.Message;
import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.axis.message.SOAPEnvelope;
import org.apache.axis.message.SOAPHeaderElement;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ddtekno.soap.e2pay.WsBillerGatewayProxyServiceLocator;
import com.ddtekno.soap.e2pay.WsBillerGatewayProxy_PortType;
import com.ddtekno.soap.e2pay.WsbgRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SOAPE2PayRequestSandbox {

	private ObjectMapper mapper = new ObjectMapper();

	public String send(@Header("service") String service, @Header("authorization") String token,
			@Header("date") String date, @Body WsbgRequest request) throws ServiceException {
		WsBillerGatewayProxyServiceLocator serviceLocator = new WsBillerGatewayProxyServiceLocator();
		
		java.net.URL endpoint;
        try {
            endpoint = new java.net.URL("https://bgtest.e2pay.co.id/axis/services/WsBillerGatewayProxy");
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }

		WsBillerGatewayProxy_PortType stub = serviceLocator.getWsBillerGatewayProxy(endpoint);

		try {
			switch (service) {
			case "purchase":
				return purchase(stub, token, date, request);

			case "checkPurchase":
				return checkPurchase(stub, token, date, request);
			default:
				throw new IllegalArgumentException("Invalid service header " + service);
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new ServiceException("Invalid Json");
		}

	}

	private String purchase(WsBillerGatewayProxy_PortType stub, String token, String date, WsbgRequest request)
			throws JsonProcessingException {
		SOAPEnvelope envelope = new SOAPEnvelope();
		Map<String, String> data = new HashMap<String, String>();

		try {
			envelope.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
			envelope.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			envelope.setAttribute("xmlns:soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
			envelope.setAttribute("xmlns:exp", "http://exposed.webservice.pacarana.infinetworks.com");

			SOAPHeaderElement auth = new SOAPHeaderElement(null, "authorization", token);
			Element e = auth.getAsDOM();
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.removeAttribute("xmlns:soapenv");
			e.removeAttribute("xmlns:soapenc");
			e.removeAttribute("xsi:type");
			e.setAttribute("xsi:type", "xsd:string");
			auth = new SOAPHeaderElement(e);

			SOAPHeaderElement dateTime = new SOAPHeaderElement(null, "date", date);
			e = dateTime.getAsDOM();
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.removeAttribute("xmlns:soapenv");
			e.removeAttribute("xmlns:soapenc");
			e.removeAttribute("xsi:type");
			e.setAttribute("xsi:type", "xsd:dateTime");
			dateTime = new SOAPHeaderElement(e);

			envelope.addHeader(auth);
			envelope.addHeader(dateTime);

			SOAPBodyElement operation = new SOAPBodyElement();
			operation.setName("exp:purchase");
			operation.setAttribute("xmlns:exp", "http://exposed.webservice.pacarana.infinetworks.com");
			operation.setAttribute("soapenv:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding");

			envelope.addBodyElement(operation);

			MessageElement req = new MessageElement();
			req.setName("req");
			req.setAttribute("xsi:type", "pac:WsbgRequest");
			req.setAttribute("xmlns:pac", "http://pacarana.infinetworks.com");
			operation.addChild(req);

			MessageElement bankId = new MessageElement(new QName("bankId"), request.getBankId());
			e = bankId.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement bankChannel = new MessageElement(new QName("bankChannel"), request.getBankChannel());
			e = bankChannel.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement bankRefNo = new MessageElement(new QName("bankRefNo"), request.getBankRefNo());
			e = bankRefNo.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement custId = new MessageElement(new QName("custId"), request.getCustId());
			e = custId.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement custAccNo = new MessageElement(new QName("custAccNo"), request.getCustAccNo());
			e = custAccNo.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement dateTrx = new MessageElement(new QName("dateTrx"), request.getDateTrx());
			e = dateTrx.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:dateTime");
			req.addChild(new MessageElement(e));

			MessageElement payeeCode = new MessageElement(new QName("payeeCode"), request.getPayeeCode());
			e = payeeCode.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement productCode = new MessageElement(new QName("productCode"), request.getProductCode());
			e = productCode.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

		} catch (SOAPException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {

			Message msg = new Message(envelope);

			org.apache.axis.message.SOAPEnvelope resp = stub.purchase(msg);

			SOAPBodyElement body = resp.getBodyByName(null, "multiRef");

			for (Object o : body.getChildren()) {
				MessageElement m = (MessageElement) o;
				data.put(m.getLocalName(), m.getValue());
			}

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mapper.writeValueAsString(data);
	}

	private String checkPurchase(WsBillerGatewayProxy_PortType stub, String token, String date, WsbgRequest request)
			throws JsonProcessingException {
		SOAPEnvelope envelope = new SOAPEnvelope();
		Map<String, String> data = new HashMap<String, String>();

		try {
			envelope.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
			envelope.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			envelope.setAttribute("xmlns:soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
			envelope.setAttribute("xmlns:exp", "http://exposed.webservice.pacarana.infinetworks.com");

			SOAPHeaderElement auth = new SOAPHeaderElement(null, "authorization", token);
			Element e = auth.getAsDOM();
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.removeAttribute("xmlns:soapenv");
			e.removeAttribute("xmlns:soapenc");
			e.removeAttribute("xsi:type");
			e.setAttribute("xsi:type", "xsd:string");
			auth = new SOAPHeaderElement(e);

			SOAPHeaderElement dateTime = new SOAPHeaderElement(null, "date", date);
			e = dateTime.getAsDOM();
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.removeAttribute("xmlns:soapenv");
			e.removeAttribute("xmlns:soapenc");
			e.removeAttribute("xsi:type");
			e.setAttribute("xsi:type", "xsd:dateTime");
			dateTime = new SOAPHeaderElement(e);

			envelope.addHeader(auth);
			envelope.addHeader(dateTime);

			SOAPBodyElement operation = new SOAPBodyElement();
			operation.setName("exp:checkPurchase");
			operation.setAttribute("xmlns:exp", "http://exposed.webservice.pacarana.infinetworks.com");
			operation.setAttribute("soapenv:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding");

			envelope.addBodyElement(operation);

			MessageElement req = new MessageElement();
			req.setName("req");
			req.setAttribute("xsi:type", "pac:WsbgRequest");
			req.setAttribute("xmlns:pac", "http://pacarana.infinetworks.com");
			operation.addChild(req);

			MessageElement bankId = new MessageElement(new QName("bankId"), request.getBankId());
			e = bankId.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement custId = new MessageElement(new QName("custId"), request.getCustId());
			e = custId.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement custRefNo = new MessageElement(new QName("custRefNo"), request.getCustRefNo());
			e = custRefNo.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement payeeCode = new MessageElement(new QName("payeeCode"), request.getPayeeCode());
			e = payeeCode.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

			MessageElement productCode = new MessageElement(new QName("productCode"), request.getProductCode());
			e = productCode.getAsDOM();
			e.removeAttribute("xsi:type");
			e.removeAttribute("xmlns:xsi");
			e.removeAttribute("xmlns:xsd");
			e.setAttribute("xsi:type", "xsd:string");
			req.addChild(new MessageElement(e));

		} catch (SOAPException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {

			Message msg = new Message(envelope);

			org.apache.axis.message.SOAPEnvelope resp = stub.purchase(msg);
			
			System.out.println(resp.getAsString());

			SOAPBodyElement body = resp.getBodyByName(null, "multiRef");

			for (Object o : body.getChildren()) {
				MessageElement m = (MessageElement) o;
				data.put(m.getLocalName(), m.getValue());
			}
			
			

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(data.isEmpty())
			data.put("message", "Internal Server Error on SOAP Call");
		
		return mapper.writeValueAsString(data);
	}

}
