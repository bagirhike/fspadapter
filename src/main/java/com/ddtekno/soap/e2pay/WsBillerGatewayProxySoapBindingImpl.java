/**
 * WsBillerGatewayProxySoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ddtekno.soap.e2pay;

import java.rmi.RemoteException;

import org.apache.axis.Message;
import org.apache.axis.message.SOAPEnvelope;

public class WsBillerGatewayProxySoapBindingImpl implements com.ddtekno.soap.e2pay.WsBillerGatewayProxy_PortType{
    public com.ddtekno.soap.e2pay.WsbgResponse inquiry(com.ddtekno.soap.e2pay.WsbgRequest req) throws java.rmi.RemoteException {
        return null;
    }

    public com.ddtekno.soap.e2pay.WsbgResponse payment(com.ddtekno.soap.e2pay.WsbgRequest req) throws java.rmi.RemoteException {
        return null;
    }

    public com.ddtekno.soap.e2pay.WsbgResponse purchase(com.ddtekno.soap.e2pay.WsbgRequest req) throws java.rmi.RemoteException {
        return null;
    }

    public com.ddtekno.soap.e2pay.WsbgAccountLedgerResponse getAccountLedger(com.ddtekno.soap.e2pay.WsbgAccountLedgerRequest req) throws java.rmi.RemoteException {
        return null;
    }

    public com.ddtekno.soap.e2pay.WsbgClientDepositResponse getDepositBalance(com.ddtekno.soap.e2pay.WsbgRequest req) throws java.rmi.RemoteException {
        return null;
    }

    public com.ddtekno.soap.e2pay.WsbgResponse checkPayment(com.ddtekno.soap.e2pay.WsbgRequest req) throws java.rmi.RemoteException {
        return null;
    }

    public com.ddtekno.soap.e2pay.WsbgResponse checkPurchase(com.ddtekno.soap.e2pay.WsbgRequest req) throws java.rmi.RemoteException {
        return null;
    }

    public com.ddtekno.soap.e2pay.WsbgPrepaidProductResponse prepaidProduct(com.ddtekno.soap.e2pay.WsbgRequest req) throws java.rmi.RemoteException {
        return null;
    }

    public com.ddtekno.soap.e2pay.WsbgPostpaidProductResponse postpaidProduct(com.ddtekno.soap.e2pay.WsbgRequest req) throws java.rmi.RemoteException {
        return null;
    }

	@Override
	public SOAPEnvelope purchase(Message msg) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SOAPEnvelope checkPurchase(Message msg) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

}
