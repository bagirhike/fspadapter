/**
 * WsBillerGatewayProxyService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ddtekno.soap.e2pay;

public interface WsBillerGatewayProxyService extends javax.xml.rpc.Service {
    public java.lang.String getWsBillerGatewayProxyAddress();

    public com.ddtekno.soap.e2pay.WsBillerGatewayProxy_PortType getWsBillerGatewayProxy() throws javax.xml.rpc.ServiceException;

    public com.ddtekno.soap.e2pay.WsBillerGatewayProxy_PortType getWsBillerGatewayProxy(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
