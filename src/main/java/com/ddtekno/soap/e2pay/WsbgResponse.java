/**
 * WsbgResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ddtekno.soap.e2pay;

public class WsbgResponse  implements java.io.Serializable {
    private java.math.BigDecimal adminFee;

    private java.lang.String bankChannel;

    private java.lang.String bankId;

    private java.lang.String bankRefNo;

    private java.math.BigDecimal billAmt;

    private java.lang.String billPeriod;

    private java.lang.String billRefNo;

    private java.lang.Byte code;

    private java.lang.String custAccNo;

    private java.lang.String custId;

    private java.lang.String custName;

    private java.lang.String custRefNo;

    private java.util.Calendar dateTrx;

    private java.lang.String description;

    private java.lang.String kwh;

    private java.lang.String message;

    private java.lang.String meterNumber;

    private java.lang.String meterStandCubication;

    private java.lang.String nominalVoucher;

    private java.math.BigDecimal paidAmt;

    private java.lang.String payeeCode;

    private java.lang.String priceCategoryPower;

    private java.lang.String pricesCategory;

    private java.lang.String productCode;

    private java.lang.String resultCode;

    private java.lang.String secretCode;

    private java.lang.String serialNumber;

    private java.lang.String standMeter;

    private java.lang.String tenor;

    private java.lang.String token;

    public WsbgResponse() {
    }

    public WsbgResponse(
           java.math.BigDecimal adminFee,
           java.lang.String bankChannel,
           java.lang.String bankId,
           java.lang.String bankRefNo,
           java.math.BigDecimal billAmt,
           java.lang.String billPeriod,
           java.lang.String billRefNo,
           java.lang.Byte code,
           java.lang.String custAccNo,
           java.lang.String custId,
           java.lang.String custName,
           java.lang.String custRefNo,
           java.util.Calendar dateTrx,
           java.lang.String description,
           java.lang.String kwh,
           java.lang.String message,
           java.lang.String meterNumber,
           java.lang.String meterStandCubication,
           java.lang.String nominalVoucher,
           java.math.BigDecimal paidAmt,
           java.lang.String payeeCode,
           java.lang.String priceCategoryPower,
           java.lang.String pricesCategory,
           java.lang.String productCode,
           java.lang.String resultCode,
           java.lang.String secretCode,
           java.lang.String serialNumber,
           java.lang.String standMeter,
           java.lang.String tenor,
           java.lang.String token) {
           this.adminFee = adminFee;
           this.bankChannel = bankChannel;
           this.bankId = bankId;
           this.bankRefNo = bankRefNo;
           this.billAmt = billAmt;
           this.billPeriod = billPeriod;
           this.billRefNo = billRefNo;
           this.code = code;
           this.custAccNo = custAccNo;
           this.custId = custId;
           this.custName = custName;
           this.custRefNo = custRefNo;
           this.dateTrx = dateTrx;
           this.description = description;
           this.kwh = kwh;
           this.message = message;
           this.meterNumber = meterNumber;
           this.meterStandCubication = meterStandCubication;
           this.nominalVoucher = nominalVoucher;
           this.paidAmt = paidAmt;
           this.payeeCode = payeeCode;
           this.priceCategoryPower = priceCategoryPower;
           this.pricesCategory = pricesCategory;
           this.productCode = productCode;
           this.resultCode = resultCode;
           this.secretCode = secretCode;
           this.serialNumber = serialNumber;
           this.standMeter = standMeter;
           this.tenor = tenor;
           this.token = token;
    }


    /**
     * Gets the adminFee value for this WsbgResponse.
     * 
     * @return adminFee
     */
    public java.math.BigDecimal getAdminFee() {
        return adminFee;
    }


    /**
     * Sets the adminFee value for this WsbgResponse.
     * 
     * @param adminFee
     */
    public void setAdminFee(java.math.BigDecimal adminFee) {
        this.adminFee = adminFee;
    }


    /**
     * Gets the bankChannel value for this WsbgResponse.
     * 
     * @return bankChannel
     */
    public java.lang.String getBankChannel() {
        return bankChannel;
    }


    /**
     * Sets the bankChannel value for this WsbgResponse.
     * 
     * @param bankChannel
     */
    public void setBankChannel(java.lang.String bankChannel) {
        this.bankChannel = bankChannel;
    }


    /**
     * Gets the bankId value for this WsbgResponse.
     * 
     * @return bankId
     */
    public java.lang.String getBankId() {
        return bankId;
    }


    /**
     * Sets the bankId value for this WsbgResponse.
     * 
     * @param bankId
     */
    public void setBankId(java.lang.String bankId) {
        this.bankId = bankId;
    }


    /**
     * Gets the bankRefNo value for this WsbgResponse.
     * 
     * @return bankRefNo
     */
    public java.lang.String getBankRefNo() {
        return bankRefNo;
    }


    /**
     * Sets the bankRefNo value for this WsbgResponse.
     * 
     * @param bankRefNo
     */
    public void setBankRefNo(java.lang.String bankRefNo) {
        this.bankRefNo = bankRefNo;
    }


    /**
     * Gets the billAmt value for this WsbgResponse.
     * 
     * @return billAmt
     */
    public java.math.BigDecimal getBillAmt() {
        return billAmt;
    }


    /**
     * Sets the billAmt value for this WsbgResponse.
     * 
     * @param billAmt
     */
    public void setBillAmt(java.math.BigDecimal billAmt) {
        this.billAmt = billAmt;
    }


    /**
     * Gets the billPeriod value for this WsbgResponse.
     * 
     * @return billPeriod
     */
    public java.lang.String getBillPeriod() {
        return billPeriod;
    }


    /**
     * Sets the billPeriod value for this WsbgResponse.
     * 
     * @param billPeriod
     */
    public void setBillPeriod(java.lang.String billPeriod) {
        this.billPeriod = billPeriod;
    }


    /**
     * Gets the billRefNo value for this WsbgResponse.
     * 
     * @return billRefNo
     */
    public java.lang.String getBillRefNo() {
        return billRefNo;
    }


    /**
     * Sets the billRefNo value for this WsbgResponse.
     * 
     * @param billRefNo
     */
    public void setBillRefNo(java.lang.String billRefNo) {
        this.billRefNo = billRefNo;
    }


    /**
     * Gets the code value for this WsbgResponse.
     * 
     * @return code
     */
    public java.lang.Byte getCode() {
        return code;
    }


    /**
     * Sets the code value for this WsbgResponse.
     * 
     * @param code
     */
    public void setCode(java.lang.Byte code) {
        this.code = code;
    }


    /**
     * Gets the custAccNo value for this WsbgResponse.
     * 
     * @return custAccNo
     */
    public java.lang.String getCustAccNo() {
        return custAccNo;
    }


    /**
     * Sets the custAccNo value for this WsbgResponse.
     * 
     * @param custAccNo
     */
    public void setCustAccNo(java.lang.String custAccNo) {
        this.custAccNo = custAccNo;
    }


    /**
     * Gets the custId value for this WsbgResponse.
     * 
     * @return custId
     */
    public java.lang.String getCustId() {
        return custId;
    }


    /**
     * Sets the custId value for this WsbgResponse.
     * 
     * @param custId
     */
    public void setCustId(java.lang.String custId) {
        this.custId = custId;
    }


    /**
     * Gets the custName value for this WsbgResponse.
     * 
     * @return custName
     */
    public java.lang.String getCustName() {
        return custName;
    }


    /**
     * Sets the custName value for this WsbgResponse.
     * 
     * @param custName
     */
    public void setCustName(java.lang.String custName) {
        this.custName = custName;
    }


    /**
     * Gets the custRefNo value for this WsbgResponse.
     * 
     * @return custRefNo
     */
    public java.lang.String getCustRefNo() {
        return custRefNo;
    }


    /**
     * Sets the custRefNo value for this WsbgResponse.
     * 
     * @param custRefNo
     */
    public void setCustRefNo(java.lang.String custRefNo) {
        this.custRefNo = custRefNo;
    }


    /**
     * Gets the dateTrx value for this WsbgResponse.
     * 
     * @return dateTrx
     */
    public java.util.Calendar getDateTrx() {
        return dateTrx;
    }


    /**
     * Sets the dateTrx value for this WsbgResponse.
     * 
     * @param dateTrx
     */
    public void setDateTrx(java.util.Calendar dateTrx) {
        this.dateTrx = dateTrx;
    }


    /**
     * Gets the description value for this WsbgResponse.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this WsbgResponse.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the kwh value for this WsbgResponse.
     * 
     * @return kwh
     */
    public java.lang.String getKwh() {
        return kwh;
    }


    /**
     * Sets the kwh value for this WsbgResponse.
     * 
     * @param kwh
     */
    public void setKwh(java.lang.String kwh) {
        this.kwh = kwh;
    }


    /**
     * Gets the message value for this WsbgResponse.
     * 
     * @return message
     */
    public java.lang.String getMessage() {
        return message;
    }


    /**
     * Sets the message value for this WsbgResponse.
     * 
     * @param message
     */
    public void setMessage(java.lang.String message) {
        this.message = message;
    }


    /**
     * Gets the meterNumber value for this WsbgResponse.
     * 
     * @return meterNumber
     */
    public java.lang.String getMeterNumber() {
        return meterNumber;
    }


    /**
     * Sets the meterNumber value for this WsbgResponse.
     * 
     * @param meterNumber
     */
    public void setMeterNumber(java.lang.String meterNumber) {
        this.meterNumber = meterNumber;
    }


    /**
     * Gets the meterStandCubication value for this WsbgResponse.
     * 
     * @return meterStandCubication
     */
    public java.lang.String getMeterStandCubication() {
        return meterStandCubication;
    }


    /**
     * Sets the meterStandCubication value for this WsbgResponse.
     * 
     * @param meterStandCubication
     */
    public void setMeterStandCubication(java.lang.String meterStandCubication) {
        this.meterStandCubication = meterStandCubication;
    }


    /**
     * Gets the nominalVoucher value for this WsbgResponse.
     * 
     * @return nominalVoucher
     */
    public java.lang.String getNominalVoucher() {
        return nominalVoucher;
    }


    /**
     * Sets the nominalVoucher value for this WsbgResponse.
     * 
     * @param nominalVoucher
     */
    public void setNominalVoucher(java.lang.String nominalVoucher) {
        this.nominalVoucher = nominalVoucher;
    }


    /**
     * Gets the paidAmt value for this WsbgResponse.
     * 
     * @return paidAmt
     */
    public java.math.BigDecimal getPaidAmt() {
        return paidAmt;
    }


    /**
     * Sets the paidAmt value for this WsbgResponse.
     * 
     * @param paidAmt
     */
    public void setPaidAmt(java.math.BigDecimal paidAmt) {
        this.paidAmt = paidAmt;
    }


    /**
     * Gets the payeeCode value for this WsbgResponse.
     * 
     * @return payeeCode
     */
    public java.lang.String getPayeeCode() {
        return payeeCode;
    }


    /**
     * Sets the payeeCode value for this WsbgResponse.
     * 
     * @param payeeCode
     */
    public void setPayeeCode(java.lang.String payeeCode) {
        this.payeeCode = payeeCode;
    }


    /**
     * Gets the priceCategoryPower value for this WsbgResponse.
     * 
     * @return priceCategoryPower
     */
    public java.lang.String getPriceCategoryPower() {
        return priceCategoryPower;
    }


    /**
     * Sets the priceCategoryPower value for this WsbgResponse.
     * 
     * @param priceCategoryPower
     */
    public void setPriceCategoryPower(java.lang.String priceCategoryPower) {
        this.priceCategoryPower = priceCategoryPower;
    }


    /**
     * Gets the pricesCategory value for this WsbgResponse.
     * 
     * @return pricesCategory
     */
    public java.lang.String getPricesCategory() {
        return pricesCategory;
    }


    /**
     * Sets the pricesCategory value for this WsbgResponse.
     * 
     * @param pricesCategory
     */
    public void setPricesCategory(java.lang.String pricesCategory) {
        this.pricesCategory = pricesCategory;
    }


    /**
     * Gets the productCode value for this WsbgResponse.
     * 
     * @return productCode
     */
    public java.lang.String getProductCode() {
        return productCode;
    }


    /**
     * Sets the productCode value for this WsbgResponse.
     * 
     * @param productCode
     */
    public void setProductCode(java.lang.String productCode) {
        this.productCode = productCode;
    }


    /**
     * Gets the resultCode value for this WsbgResponse.
     * 
     * @return resultCode
     */
    public java.lang.String getResultCode() {
        return resultCode;
    }


    /**
     * Sets the resultCode value for this WsbgResponse.
     * 
     * @param resultCode
     */
    public void setResultCode(java.lang.String resultCode) {
        this.resultCode = resultCode;
    }


    /**
     * Gets the secretCode value for this WsbgResponse.
     * 
     * @return secretCode
     */
    public java.lang.String getSecretCode() {
        return secretCode;
    }


    /**
     * Sets the secretCode value for this WsbgResponse.
     * 
     * @param secretCode
     */
    public void setSecretCode(java.lang.String secretCode) {
        this.secretCode = secretCode;
    }


    /**
     * Gets the serialNumber value for this WsbgResponse.
     * 
     * @return serialNumber
     */
    public java.lang.String getSerialNumber() {
        return serialNumber;
    }


    /**
     * Sets the serialNumber value for this WsbgResponse.
     * 
     * @param serialNumber
     */
    public void setSerialNumber(java.lang.String serialNumber) {
        this.serialNumber = serialNumber;
    }


    /**
     * Gets the standMeter value for this WsbgResponse.
     * 
     * @return standMeter
     */
    public java.lang.String getStandMeter() {
        return standMeter;
    }


    /**
     * Sets the standMeter value for this WsbgResponse.
     * 
     * @param standMeter
     */
    public void setStandMeter(java.lang.String standMeter) {
        this.standMeter = standMeter;
    }


    /**
     * Gets the tenor value for this WsbgResponse.
     * 
     * @return tenor
     */
    public java.lang.String getTenor() {
        return tenor;
    }


    /**
     * Sets the tenor value for this WsbgResponse.
     * 
     * @param tenor
     */
    public void setTenor(java.lang.String tenor) {
        this.tenor = tenor;
    }


    /**
     * Gets the token value for this WsbgResponse.
     * 
     * @return token
     */
    public java.lang.String getToken() {
        return token;
    }


    /**
     * Sets the token value for this WsbgResponse.
     * 
     * @param token
     */
    public void setToken(java.lang.String token) {
        this.token = token;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WsbgResponse)) return false;
        WsbgResponse other = (WsbgResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.adminFee==null && other.getAdminFee()==null) || 
             (this.adminFee!=null &&
              this.adminFee.equals(other.getAdminFee()))) &&
            ((this.bankChannel==null && other.getBankChannel()==null) || 
             (this.bankChannel!=null &&
              this.bankChannel.equals(other.getBankChannel()))) &&
            ((this.bankId==null && other.getBankId()==null) || 
             (this.bankId!=null &&
              this.bankId.equals(other.getBankId()))) &&
            ((this.bankRefNo==null && other.getBankRefNo()==null) || 
             (this.bankRefNo!=null &&
              this.bankRefNo.equals(other.getBankRefNo()))) &&
            ((this.billAmt==null && other.getBillAmt()==null) || 
             (this.billAmt!=null &&
              this.billAmt.equals(other.getBillAmt()))) &&
            ((this.billPeriod==null && other.getBillPeriod()==null) || 
             (this.billPeriod!=null &&
              this.billPeriod.equals(other.getBillPeriod()))) &&
            ((this.billRefNo==null && other.getBillRefNo()==null) || 
             (this.billRefNo!=null &&
              this.billRefNo.equals(other.getBillRefNo()))) &&
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.custAccNo==null && other.getCustAccNo()==null) || 
             (this.custAccNo!=null &&
              this.custAccNo.equals(other.getCustAccNo()))) &&
            ((this.custId==null && other.getCustId()==null) || 
             (this.custId!=null &&
              this.custId.equals(other.getCustId()))) &&
            ((this.custName==null && other.getCustName()==null) || 
             (this.custName!=null &&
              this.custName.equals(other.getCustName()))) &&
            ((this.custRefNo==null && other.getCustRefNo()==null) || 
             (this.custRefNo!=null &&
              this.custRefNo.equals(other.getCustRefNo()))) &&
            ((this.dateTrx==null && other.getDateTrx()==null) || 
             (this.dateTrx!=null &&
              this.dateTrx.equals(other.getDateTrx()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.kwh==null && other.getKwh()==null) || 
             (this.kwh!=null &&
              this.kwh.equals(other.getKwh()))) &&
            ((this.message==null && other.getMessage()==null) || 
             (this.message!=null &&
              this.message.equals(other.getMessage()))) &&
            ((this.meterNumber==null && other.getMeterNumber()==null) || 
             (this.meterNumber!=null &&
              this.meterNumber.equals(other.getMeterNumber()))) &&
            ((this.meterStandCubication==null && other.getMeterStandCubication()==null) || 
             (this.meterStandCubication!=null &&
              this.meterStandCubication.equals(other.getMeterStandCubication()))) &&
            ((this.nominalVoucher==null && other.getNominalVoucher()==null) || 
             (this.nominalVoucher!=null &&
              this.nominalVoucher.equals(other.getNominalVoucher()))) &&
            ((this.paidAmt==null && other.getPaidAmt()==null) || 
             (this.paidAmt!=null &&
              this.paidAmt.equals(other.getPaidAmt()))) &&
            ((this.payeeCode==null && other.getPayeeCode()==null) || 
             (this.payeeCode!=null &&
              this.payeeCode.equals(other.getPayeeCode()))) &&
            ((this.priceCategoryPower==null && other.getPriceCategoryPower()==null) || 
             (this.priceCategoryPower!=null &&
              this.priceCategoryPower.equals(other.getPriceCategoryPower()))) &&
            ((this.pricesCategory==null && other.getPricesCategory()==null) || 
             (this.pricesCategory!=null &&
              this.pricesCategory.equals(other.getPricesCategory()))) &&
            ((this.productCode==null && other.getProductCode()==null) || 
             (this.productCode!=null &&
              this.productCode.equals(other.getProductCode()))) &&
            ((this.resultCode==null && other.getResultCode()==null) || 
             (this.resultCode!=null &&
              this.resultCode.equals(other.getResultCode()))) &&
            ((this.secretCode==null && other.getSecretCode()==null) || 
             (this.secretCode!=null &&
              this.secretCode.equals(other.getSecretCode()))) &&
            ((this.serialNumber==null && other.getSerialNumber()==null) || 
             (this.serialNumber!=null &&
              this.serialNumber.equals(other.getSerialNumber()))) &&
            ((this.standMeter==null && other.getStandMeter()==null) || 
             (this.standMeter!=null &&
              this.standMeter.equals(other.getStandMeter()))) &&
            ((this.tenor==null && other.getTenor()==null) || 
             (this.tenor!=null &&
              this.tenor.equals(other.getTenor()))) &&
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAdminFee() != null) {
            _hashCode += getAdminFee().hashCode();
        }
        if (getBankChannel() != null) {
            _hashCode += getBankChannel().hashCode();
        }
        if (getBankId() != null) {
            _hashCode += getBankId().hashCode();
        }
        if (getBankRefNo() != null) {
            _hashCode += getBankRefNo().hashCode();
        }
        if (getBillAmt() != null) {
            _hashCode += getBillAmt().hashCode();
        }
        if (getBillPeriod() != null) {
            _hashCode += getBillPeriod().hashCode();
        }
        if (getBillRefNo() != null) {
            _hashCode += getBillRefNo().hashCode();
        }
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getCustAccNo() != null) {
            _hashCode += getCustAccNo().hashCode();
        }
        if (getCustId() != null) {
            _hashCode += getCustId().hashCode();
        }
        if (getCustName() != null) {
            _hashCode += getCustName().hashCode();
        }
        if (getCustRefNo() != null) {
            _hashCode += getCustRefNo().hashCode();
        }
        if (getDateTrx() != null) {
            _hashCode += getDateTrx().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getKwh() != null) {
            _hashCode += getKwh().hashCode();
        }
        if (getMessage() != null) {
            _hashCode += getMessage().hashCode();
        }
        if (getMeterNumber() != null) {
            _hashCode += getMeterNumber().hashCode();
        }
        if (getMeterStandCubication() != null) {
            _hashCode += getMeterStandCubication().hashCode();
        }
        if (getNominalVoucher() != null) {
            _hashCode += getNominalVoucher().hashCode();
        }
        if (getPaidAmt() != null) {
            _hashCode += getPaidAmt().hashCode();
        }
        if (getPayeeCode() != null) {
            _hashCode += getPayeeCode().hashCode();
        }
        if (getPriceCategoryPower() != null) {
            _hashCode += getPriceCategoryPower().hashCode();
        }
        if (getPricesCategory() != null) {
            _hashCode += getPricesCategory().hashCode();
        }
        if (getProductCode() != null) {
            _hashCode += getProductCode().hashCode();
        }
        if (getResultCode() != null) {
            _hashCode += getResultCode().hashCode();
        }
        if (getSecretCode() != null) {
            _hashCode += getSecretCode().hashCode();
        }
        if (getSerialNumber() != null) {
            _hashCode += getSerialNumber().hashCode();
        }
        if (getStandMeter() != null) {
            _hashCode += getStandMeter().hashCode();
        }
        if (getTenor() != null) {
            _hashCode += getTenor().hashCode();
        }
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WsbgResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adminFee");
        elemField.setXmlName(new javax.xml.namespace.QName("", "adminFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankChannel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bankChannel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bankId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankRefNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bankRefNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "billAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billPeriod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "billPeriod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billRefNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "billRefNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "byte"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custAccNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custAccNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custRefNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custRefNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateTrx");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateTrx"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kwh");
        elemField.setXmlName(new javax.xml.namespace.QName("", "kwh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("message");
        elemField.setXmlName(new javax.xml.namespace.QName("", "message"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("meterNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "meterNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("meterStandCubication");
        elemField.setXmlName(new javax.xml.namespace.QName("", "meterStandCubication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nominalVoucher");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nominalVoucher"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paidAmt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paidAmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payeeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payeeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priceCategoryPower");
        elemField.setXmlName(new javax.xml.namespace.QName("", "priceCategoryPower"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pricesCategory");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pricesCategory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("secretCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "secretCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serialNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "serialNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("standMeter");
        elemField.setXmlName(new javax.xml.namespace.QName("", "standMeter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tenor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tenor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("", "token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
