/**
 * WsbgAccountLedgerResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ddtekno.soap.e2pay;

public class WsbgAccountLedgerResponse  implements java.io.Serializable {
    private java.lang.String bankId;

    private java.lang.Byte code;

    private java.lang.String countData;

    private java.lang.Object[] data;

    private java.lang.String message;

    private java.lang.String page;

    private java.lang.String resultCode;

    private java.lang.String totalData;

    public WsbgAccountLedgerResponse() {
    }

    public WsbgAccountLedgerResponse(
           java.lang.String bankId,
           java.lang.Byte code,
           java.lang.String countData,
           java.lang.Object[] data,
           java.lang.String message,
           java.lang.String page,
           java.lang.String resultCode,
           java.lang.String totalData) {
           this.bankId = bankId;
           this.code = code;
           this.countData = countData;
           this.data = data;
           this.message = message;
           this.page = page;
           this.resultCode = resultCode;
           this.totalData = totalData;
    }


    /**
     * Gets the bankId value for this WsbgAccountLedgerResponse.
     * 
     * @return bankId
     */
    public java.lang.String getBankId() {
        return bankId;
    }


    /**
     * Sets the bankId value for this WsbgAccountLedgerResponse.
     * 
     * @param bankId
     */
    public void setBankId(java.lang.String bankId) {
        this.bankId = bankId;
    }


    /**
     * Gets the code value for this WsbgAccountLedgerResponse.
     * 
     * @return code
     */
    public java.lang.Byte getCode() {
        return code;
    }


    /**
     * Sets the code value for this WsbgAccountLedgerResponse.
     * 
     * @param code
     */
    public void setCode(java.lang.Byte code) {
        this.code = code;
    }


    /**
     * Gets the countData value for this WsbgAccountLedgerResponse.
     * 
     * @return countData
     */
    public java.lang.String getCountData() {
        return countData;
    }


    /**
     * Sets the countData value for this WsbgAccountLedgerResponse.
     * 
     * @param countData
     */
    public void setCountData(java.lang.String countData) {
        this.countData = countData;
    }


    /**
     * Gets the data value for this WsbgAccountLedgerResponse.
     * 
     * @return data
     */
    public java.lang.Object[] getData() {
        return data;
    }


    /**
     * Sets the data value for this WsbgAccountLedgerResponse.
     * 
     * @param data
     */
    public void setData(java.lang.Object[] data) {
        this.data = data;
    }


    /**
     * Gets the message value for this WsbgAccountLedgerResponse.
     * 
     * @return message
     */
    public java.lang.String getMessage() {
        return message;
    }


    /**
     * Sets the message value for this WsbgAccountLedgerResponse.
     * 
     * @param message
     */
    public void setMessage(java.lang.String message) {
        this.message = message;
    }


    /**
     * Gets the page value for this WsbgAccountLedgerResponse.
     * 
     * @return page
     */
    public java.lang.String getPage() {
        return page;
    }


    /**
     * Sets the page value for this WsbgAccountLedgerResponse.
     * 
     * @param page
     */
    public void setPage(java.lang.String page) {
        this.page = page;
    }


    /**
     * Gets the resultCode value for this WsbgAccountLedgerResponse.
     * 
     * @return resultCode
     */
    public java.lang.String getResultCode() {
        return resultCode;
    }


    /**
     * Sets the resultCode value for this WsbgAccountLedgerResponse.
     * 
     * @param resultCode
     */
    public void setResultCode(java.lang.String resultCode) {
        this.resultCode = resultCode;
    }


    /**
     * Gets the totalData value for this WsbgAccountLedgerResponse.
     * 
     * @return totalData
     */
    public java.lang.String getTotalData() {
        return totalData;
    }


    /**
     * Sets the totalData value for this WsbgAccountLedgerResponse.
     * 
     * @param totalData
     */
    public void setTotalData(java.lang.String totalData) {
        this.totalData = totalData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WsbgAccountLedgerResponse)) return false;
        WsbgAccountLedgerResponse other = (WsbgAccountLedgerResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bankId==null && other.getBankId()==null) || 
             (this.bankId!=null &&
              this.bankId.equals(other.getBankId()))) &&
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.countData==null && other.getCountData()==null) || 
             (this.countData!=null &&
              this.countData.equals(other.getCountData()))) &&
            ((this.data==null && other.getData()==null) || 
             (this.data!=null &&
              java.util.Arrays.equals(this.data, other.getData()))) &&
            ((this.message==null && other.getMessage()==null) || 
             (this.message!=null &&
              this.message.equals(other.getMessage()))) &&
            ((this.page==null && other.getPage()==null) || 
             (this.page!=null &&
              this.page.equals(other.getPage()))) &&
            ((this.resultCode==null && other.getResultCode()==null) || 
             (this.resultCode!=null &&
              this.resultCode.equals(other.getResultCode()))) &&
            ((this.totalData==null && other.getTotalData()==null) || 
             (this.totalData!=null &&
              this.totalData.equals(other.getTotalData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBankId() != null) {
            _hashCode += getBankId().hashCode();
        }
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getCountData() != null) {
            _hashCode += getCountData().hashCode();
        }
        if (getData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMessage() != null) {
            _hashCode += getMessage().hashCode();
        }
        if (getPage() != null) {
            _hashCode += getPage().hashCode();
        }
        if (getResultCode() != null) {
            _hashCode += getResultCode().hashCode();
        }
        if (getTotalData() != null) {
            _hashCode += getTotalData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WsbgAccountLedgerResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgAccountLedgerResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bankId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "byte"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countData");
        elemField.setXmlName(new javax.xml.namespace.QName("", "countData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("", "data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("message");
        elemField.setXmlName(new javax.xml.namespace.QName("", "message"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("page");
        elemField.setXmlName(new javax.xml.namespace.QName("", "page"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalData");
        elemField.setXmlName(new javax.xml.namespace.QName("", "totalData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
