/**
 * WsBillerGatewayProxyServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ddtekno.soap.e2pay;

public class WsBillerGatewayProxyServiceLocator extends org.apache.axis.client.Service implements com.ddtekno.soap.e2pay.WsBillerGatewayProxyService {

    public WsBillerGatewayProxyServiceLocator() {
    }


    public WsBillerGatewayProxyServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WsBillerGatewayProxyServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WsBillerGatewayProxy
    private java.lang.String WsBillerGatewayProxy_address = "https://bgtest.e2pay.co.id/axis/services/WsBillerGatewayProxy";

    public java.lang.String getWsBillerGatewayProxyAddress() {
        return WsBillerGatewayProxy_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WsBillerGatewayProxyWSDDServiceName = "WsBillerGatewayProxy";

    public java.lang.String getWsBillerGatewayProxyWSDDServiceName() {
        return WsBillerGatewayProxyWSDDServiceName;
    }

    public void setWsBillerGatewayProxyWSDDServiceName(java.lang.String name) {
        WsBillerGatewayProxyWSDDServiceName = name;
    }

    public com.ddtekno.soap.e2pay.WsBillerGatewayProxy_PortType getWsBillerGatewayProxy() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WsBillerGatewayProxy_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWsBillerGatewayProxy(endpoint);
    }

    public com.ddtekno.soap.e2pay.WsBillerGatewayProxy_PortType getWsBillerGatewayProxy(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.ddtekno.soap.e2pay.WsBillerGatewayProxySoapBindingStub _stub = new com.ddtekno.soap.e2pay.WsBillerGatewayProxySoapBindingStub(portAddress, this);
            _stub.setPortName(getWsBillerGatewayProxyWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWsBillerGatewayProxyEndpointAddress(java.lang.String address) {
        WsBillerGatewayProxy_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.ddtekno.soap.e2pay.WsBillerGatewayProxy_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.ddtekno.soap.e2pay.WsBillerGatewayProxySoapBindingStub _stub = new com.ddtekno.soap.e2pay.WsBillerGatewayProxySoapBindingStub(new java.net.URL(WsBillerGatewayProxy_address), this);
                _stub.setPortName(getWsBillerGatewayProxyWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WsBillerGatewayProxy".equals(inputPortName)) {
            return getWsBillerGatewayProxy();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsBillerGatewayProxyService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsBillerGatewayProxy"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WsBillerGatewayProxy".equals(portName)) {
            setWsBillerGatewayProxyEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
