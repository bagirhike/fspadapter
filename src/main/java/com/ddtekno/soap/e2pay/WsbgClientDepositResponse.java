/**
 * WsbgClientDepositResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ddtekno.soap.e2pay;

public class WsbgClientDepositResponse  implements java.io.Serializable {
    private java.lang.String bankId;

    private java.lang.Byte code;

    private java.util.Calendar dateTrx;

    private java.math.BigDecimal depositAmount;

    private java.lang.String message;

    private java.lang.String resultCode;

    public WsbgClientDepositResponse() {
    }

    public WsbgClientDepositResponse(
           java.lang.String bankId,
           java.lang.Byte code,
           java.util.Calendar dateTrx,
           java.math.BigDecimal depositAmount,
           java.lang.String message,
           java.lang.String resultCode) {
           this.bankId = bankId;
           this.code = code;
           this.dateTrx = dateTrx;
           this.depositAmount = depositAmount;
           this.message = message;
           this.resultCode = resultCode;
    }


    /**
     * Gets the bankId value for this WsbgClientDepositResponse.
     * 
     * @return bankId
     */
    public java.lang.String getBankId() {
        return bankId;
    }


    /**
     * Sets the bankId value for this WsbgClientDepositResponse.
     * 
     * @param bankId
     */
    public void setBankId(java.lang.String bankId) {
        this.bankId = bankId;
    }


    /**
     * Gets the code value for this WsbgClientDepositResponse.
     * 
     * @return code
     */
    public java.lang.Byte getCode() {
        return code;
    }


    /**
     * Sets the code value for this WsbgClientDepositResponse.
     * 
     * @param code
     */
    public void setCode(java.lang.Byte code) {
        this.code = code;
    }


    /**
     * Gets the dateTrx value for this WsbgClientDepositResponse.
     * 
     * @return dateTrx
     */
    public java.util.Calendar getDateTrx() {
        return dateTrx;
    }


    /**
     * Sets the dateTrx value for this WsbgClientDepositResponse.
     * 
     * @param dateTrx
     */
    public void setDateTrx(java.util.Calendar dateTrx) {
        this.dateTrx = dateTrx;
    }


    /**
     * Gets the depositAmount value for this WsbgClientDepositResponse.
     * 
     * @return depositAmount
     */
    public java.math.BigDecimal getDepositAmount() {
        return depositAmount;
    }


    /**
     * Sets the depositAmount value for this WsbgClientDepositResponse.
     * 
     * @param depositAmount
     */
    public void setDepositAmount(java.math.BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }


    /**
     * Gets the message value for this WsbgClientDepositResponse.
     * 
     * @return message
     */
    public java.lang.String getMessage() {
        return message;
    }


    /**
     * Sets the message value for this WsbgClientDepositResponse.
     * 
     * @param message
     */
    public void setMessage(java.lang.String message) {
        this.message = message;
    }


    /**
     * Gets the resultCode value for this WsbgClientDepositResponse.
     * 
     * @return resultCode
     */
    public java.lang.String getResultCode() {
        return resultCode;
    }


    /**
     * Sets the resultCode value for this WsbgClientDepositResponse.
     * 
     * @param resultCode
     */
    public void setResultCode(java.lang.String resultCode) {
        this.resultCode = resultCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WsbgClientDepositResponse)) return false;
        WsbgClientDepositResponse other = (WsbgClientDepositResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bankId==null && other.getBankId()==null) || 
             (this.bankId!=null &&
              this.bankId.equals(other.getBankId()))) &&
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.dateTrx==null && other.getDateTrx()==null) || 
             (this.dateTrx!=null &&
              this.dateTrx.equals(other.getDateTrx()))) &&
            ((this.depositAmount==null && other.getDepositAmount()==null) || 
             (this.depositAmount!=null &&
              this.depositAmount.equals(other.getDepositAmount()))) &&
            ((this.message==null && other.getMessage()==null) || 
             (this.message!=null &&
              this.message.equals(other.getMessage()))) &&
            ((this.resultCode==null && other.getResultCode()==null) || 
             (this.resultCode!=null &&
              this.resultCode.equals(other.getResultCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBankId() != null) {
            _hashCode += getBankId().hashCode();
        }
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getDateTrx() != null) {
            _hashCode += getDateTrx().hashCode();
        }
        if (getDepositAmount() != null) {
            _hashCode += getDepositAmount().hashCode();
        }
        if (getMessage() != null) {
            _hashCode += getMessage().hashCode();
        }
        if (getResultCode() != null) {
            _hashCode += getResultCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WsbgClientDepositResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgClientDepositResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bankId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "byte"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateTrx");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateTrx"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("depositAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "depositAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("message");
        elemField.setXmlName(new javax.xml.namespace.QName("", "message"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
