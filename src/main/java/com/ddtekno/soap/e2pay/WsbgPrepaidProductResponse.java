/**
 * WsbgPrepaidProductResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ddtekno.soap.e2pay;

public class WsbgPrepaidProductResponse  implements java.io.Serializable {
    private java.lang.Byte code;

    private java.lang.Object[] data;

    private java.util.Calendar dateTrx;

    private java.lang.String message;

    private java.lang.String resultCode;

    public WsbgPrepaidProductResponse() {
    }

    public WsbgPrepaidProductResponse(
           java.lang.Byte code,
           java.lang.Object[] data,
           java.util.Calendar dateTrx,
           java.lang.String message,
           java.lang.String resultCode) {
           this.code = code;
           this.data = data;
           this.dateTrx = dateTrx;
           this.message = message;
           this.resultCode = resultCode;
    }


    /**
     * Gets the code value for this WsbgPrepaidProductResponse.
     * 
     * @return code
     */
    public java.lang.Byte getCode() {
        return code;
    }


    /**
     * Sets the code value for this WsbgPrepaidProductResponse.
     * 
     * @param code
     */
    public void setCode(java.lang.Byte code) {
        this.code = code;
    }


    /**
     * Gets the data value for this WsbgPrepaidProductResponse.
     * 
     * @return data
     */
    public java.lang.Object[] getData() {
        return data;
    }


    /**
     * Sets the data value for this WsbgPrepaidProductResponse.
     * 
     * @param data
     */
    public void setData(java.lang.Object[] data) {
        this.data = data;
    }


    /**
     * Gets the dateTrx value for this WsbgPrepaidProductResponse.
     * 
     * @return dateTrx
     */
    public java.util.Calendar getDateTrx() {
        return dateTrx;
    }


    /**
     * Sets the dateTrx value for this WsbgPrepaidProductResponse.
     * 
     * @param dateTrx
     */
    public void setDateTrx(java.util.Calendar dateTrx) {
        this.dateTrx = dateTrx;
    }


    /**
     * Gets the message value for this WsbgPrepaidProductResponse.
     * 
     * @return message
     */
    public java.lang.String getMessage() {
        return message;
    }


    /**
     * Sets the message value for this WsbgPrepaidProductResponse.
     * 
     * @param message
     */
    public void setMessage(java.lang.String message) {
        this.message = message;
    }


    /**
     * Gets the resultCode value for this WsbgPrepaidProductResponse.
     * 
     * @return resultCode
     */
    public java.lang.String getResultCode() {
        return resultCode;
    }


    /**
     * Sets the resultCode value for this WsbgPrepaidProductResponse.
     * 
     * @param resultCode
     */
    public void setResultCode(java.lang.String resultCode) {
        this.resultCode = resultCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WsbgPrepaidProductResponse)) return false;
        WsbgPrepaidProductResponse other = (WsbgPrepaidProductResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.data==null && other.getData()==null) || 
             (this.data!=null &&
              java.util.Arrays.equals(this.data, other.getData()))) &&
            ((this.dateTrx==null && other.getDateTrx()==null) || 
             (this.dateTrx!=null &&
              this.dateTrx.equals(other.getDateTrx()))) &&
            ((this.message==null && other.getMessage()==null) || 
             (this.message!=null &&
              this.message.equals(other.getMessage()))) &&
            ((this.resultCode==null && other.getResultCode()==null) || 
             (this.resultCode!=null &&
              this.resultCode.equals(other.getResultCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDateTrx() != null) {
            _hashCode += getDateTrx().hashCode();
        }
        if (getMessage() != null) {
            _hashCode += getMessage().hashCode();
        }
        if (getResultCode() != null) {
            _hashCode += getResultCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WsbgPrepaidProductResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgPrepaidProductResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "byte"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("", "data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateTrx");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateTrx"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("message");
        elemField.setXmlName(new javax.xml.namespace.QName("", "message"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
