/**
 * WsBillerGatewayProxySoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ddtekno.soap.e2pay;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;

import org.apache.axis.Message;

public class WsBillerGatewayProxySoapBindingStub extends org.apache.axis.client.Stub
		implements com.ddtekno.soap.e2pay.WsBillerGatewayProxy_PortType {
	private java.util.Vector cachedSerClasses = new java.util.Vector();
	private java.util.Vector cachedSerQNames = new java.util.Vector();
	private java.util.Vector cachedSerFactories = new java.util.Vector();
	private java.util.Vector cachedDeserFactories = new java.util.Vector();

	static org.apache.axis.description.OperationDesc[] _operations;

	static {
		_operations = new org.apache.axis.description.OperationDesc[9];
		_initOperationDesc1();
	}

	private static void _initOperationDesc1() {
		org.apache.axis.description.OperationDesc oper;
		org.apache.axis.description.ParameterDesc param;
		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("inquiry");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "req"),
				org.apache.axis.description.ParameterDesc.IN,
				new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgRequest"),
				com.ddtekno.soap.e2pay.WsbgRequest.class, false, false);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgResponse"));
		oper.setReturnClass(com.ddtekno.soap.e2pay.WsbgResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("", "inquiryReturn"));
		oper.setStyle(org.apache.axis.constants.Style.RPC);
		oper.setUse(org.apache.axis.constants.Use.ENCODED);
		_operations[0] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("payment");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "req"),
				org.apache.axis.description.ParameterDesc.IN,
				new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgRequest"),
				com.ddtekno.soap.e2pay.WsbgRequest.class, false, false);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgResponse"));
		oper.setReturnClass(com.ddtekno.soap.e2pay.WsbgResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("", "paymentReturn"));
		oper.setStyle(org.apache.axis.constants.Style.RPC);
		oper.setUse(org.apache.axis.constants.Use.ENCODED);
		_operations[1] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("purchase");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "req"),
				org.apache.axis.description.ParameterDesc.IN,
				new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgRequest"),
				com.ddtekno.soap.e2pay.WsbgRequest.class, false, false);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgResponse"));
		oper.setReturnClass(com.ddtekno.soap.e2pay.WsbgResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("", "purchaseReturn"));
		oper.setStyle(org.apache.axis.constants.Style.DEFAULT);
		oper.setUse(org.apache.axis.constants.Use.DEFAULT);
		_operations[2] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("getAccountLedger");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "req"),
				org.apache.axis.description.ParameterDesc.IN,
				new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgAccountLedgerRequest"),
				com.ddtekno.soap.e2pay.WsbgAccountLedgerRequest.class, false, false);
		oper.addParameter(param);
		oper.setReturnType(
				new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgAccountLedgerResponse"));
		oper.setReturnClass(com.ddtekno.soap.e2pay.WsbgAccountLedgerResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("", "getAccountLedgerReturn"));
		oper.setStyle(org.apache.axis.constants.Style.RPC);
		oper.setUse(org.apache.axis.constants.Use.ENCODED);
		_operations[3] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("getDepositBalance");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "req"),
				org.apache.axis.description.ParameterDesc.IN,
				new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgRequest"),
				com.ddtekno.soap.e2pay.WsbgRequest.class, false, false);
		oper.addParameter(param);
		oper.setReturnType(
				new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgClientDepositResponse"));
		oper.setReturnClass(com.ddtekno.soap.e2pay.WsbgClientDepositResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("", "getDepositBalanceReturn"));
		oper.setStyle(org.apache.axis.constants.Style.RPC);
		oper.setUse(org.apache.axis.constants.Use.ENCODED);
		_operations[4] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("checkPayment");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "req"),
				org.apache.axis.description.ParameterDesc.IN,
				new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgRequest"),
				com.ddtekno.soap.e2pay.WsbgRequest.class, false, false);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgResponse"));
		oper.setReturnClass(com.ddtekno.soap.e2pay.WsbgResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("", "checkPaymentReturn"));
		oper.setStyle(org.apache.axis.constants.Style.RPC);
		oper.setUse(org.apache.axis.constants.Use.ENCODED);
		_operations[5] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("checkPurchase");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "req"),
				org.apache.axis.description.ParameterDesc.IN,
				new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgRequest"),
				com.ddtekno.soap.e2pay.WsbgRequest.class, false, false);
		oper.addParameter(param);
		oper.setReturnType(new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgResponse"));
		oper.setReturnClass(com.ddtekno.soap.e2pay.WsbgResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("", "checkPurchaseReturn"));
		oper.setStyle(org.apache.axis.constants.Style.RPC);
		oper.setUse(org.apache.axis.constants.Use.ENCODED);
		_operations[6] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("prepaidProduct");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "req"),
				org.apache.axis.description.ParameterDesc.IN,
				new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgRequest"),
				com.ddtekno.soap.e2pay.WsbgRequest.class, false, false);
		oper.addParameter(param);
		oper.setReturnType(
				new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgPrepaidProductResponse"));
		oper.setReturnClass(com.ddtekno.soap.e2pay.WsbgPrepaidProductResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("", "prepaidProductReturn"));
		oper.setStyle(org.apache.axis.constants.Style.RPC);
		oper.setUse(org.apache.axis.constants.Use.ENCODED);
		_operations[7] = oper;

		oper = new org.apache.axis.description.OperationDesc();
		oper.setName("postpaidProduct");
		param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "req"),
				org.apache.axis.description.ParameterDesc.IN,
				new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgRequest"),
				com.ddtekno.soap.e2pay.WsbgRequest.class, false, false);
		oper.addParameter(param);
		oper.setReturnType(
				new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgPostpaidProductResponse"));
		oper.setReturnClass(com.ddtekno.soap.e2pay.WsbgPostpaidProductResponse.class);
		oper.setReturnQName(new javax.xml.namespace.QName("", "postpaidProductReturn"));
		oper.setStyle(org.apache.axis.constants.Style.RPC);
		oper.setUse(org.apache.axis.constants.Use.ENCODED);
		_operations[8] = oper;

	}

	public WsBillerGatewayProxySoapBindingStub() throws org.apache.axis.AxisFault {
		this(null);
	}

	public WsBillerGatewayProxySoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service)
			throws org.apache.axis.AxisFault {
		this(service);
		super.cachedEndpoint = endpointURL;
	}

	public WsBillerGatewayProxySoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
		if (service == null) {
			super.service = new org.apache.axis.client.Service();
		} else {
			super.service = service;
		}
		((org.apache.axis.client.Service) super.service).setTypeMappingVersion("1.1");
		java.lang.Class cls;
		javax.xml.namespace.QName qName;
		javax.xml.namespace.QName qName2;
		java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
		java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
		java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
		java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
		java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
		java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
		java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
		java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
		java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
		java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
		qName = new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "ArrayOf_xsd_anyType");
		cachedSerQNames.add(qName);
		cls = java.lang.Object[].class;
		cachedSerClasses.add(cls);
		qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType");
		qName2 = null;
		cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
		cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

		qName = new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgAccountLedgerRequest");
		cachedSerQNames.add(qName);
		cls = com.ddtekno.soap.e2pay.WsbgAccountLedgerRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgAccountLedgerResponse");
		cachedSerQNames.add(qName);
		cls = com.ddtekno.soap.e2pay.WsbgAccountLedgerResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgClientDepositResponse");
		cachedSerQNames.add(qName);
		cls = com.ddtekno.soap.e2pay.WsbgClientDepositResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgPostpaidProductResponse");
		cachedSerQNames.add(qName);
		cls = com.ddtekno.soap.e2pay.WsbgPostpaidProductResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgPrepaidProductResponse");
		cachedSerQNames.add(qName);
		cls = com.ddtekno.soap.e2pay.WsbgPrepaidProductResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgRequest");
		cachedSerQNames.add(qName);
		cls = com.ddtekno.soap.e2pay.WsbgRequest.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

		qName = new javax.xml.namespace.QName("http://pacarana.infinetworks.com", "WsbgResponse");
		cachedSerQNames.add(qName);
		cls = com.ddtekno.soap.e2pay.WsbgResponse.class;
		cachedSerClasses.add(cls);
		cachedSerFactories.add(beansf);
		cachedDeserFactories.add(beandf);

	}

	protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
		try {
			org.apache.axis.client.Call _call = super._createCall();
			if (super.maintainSessionSet) {
				_call.setMaintainSession(super.maintainSession);
			}
			if (super.cachedUsername != null) {
				_call.setUsername(super.cachedUsername);
			}
			if (super.cachedPassword != null) {
				_call.setPassword(super.cachedPassword);
			}
			if (super.cachedEndpoint != null) {
				_call.setTargetEndpointAddress(super.cachedEndpoint);
			}
			if (super.cachedTimeout != null) {
				_call.setTimeout(super.cachedTimeout);
			}
			if (super.cachedPortName != null) {
				_call.setPortName(super.cachedPortName);
			}
			java.util.Enumeration keys = super.cachedProperties.keys();
			while (keys.hasMoreElements()) {
				java.lang.String key = (java.lang.String) keys.nextElement();
				_call.setProperty(key, super.cachedProperties.get(key));
			}
			// All the type mapping information is registered
			// when the first call is made.
			// The type mapping information is actually registered in
			// the TypeMappingRegistry of the service, which
			// is the reason why registration is only needed for the first call.
			synchronized (this) {
				if (firstCall()) {
					// must set encoding style before registering serializers
					_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
					_call.setEncodingStyle(org.apache.axis.Constants.URI_SOAP11_ENC);
					for (int i = 0; i < cachedSerFactories.size(); ++i) {
						java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
						javax.xml.namespace.QName qName = (javax.xml.namespace.QName) cachedSerQNames.get(i);
						java.lang.Object x = cachedSerFactories.get(i);
						if (x instanceof Class) {
							java.lang.Class sf = (java.lang.Class) cachedSerFactories.get(i);
							java.lang.Class df = (java.lang.Class) cachedDeserFactories.get(i);
							_call.registerTypeMapping(cls, qName, sf, df, false);
						} else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
							org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory) cachedSerFactories
									.get(i);
							org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory) cachedDeserFactories
									.get(i);
							_call.registerTypeMapping(cls, qName, sf, df, false);
						}
					}
				}
			}
			return _call;
		} catch (java.lang.Throwable _t) {
			throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
		}
	}

	public com.ddtekno.soap.e2pay.WsbgResponse inquiry(com.ddtekno.soap.e2pay.WsbgRequest req)
			throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[0]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("");
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(
				new javax.xml.namespace.QName("http://exposed.webservice.pacarana.infinetworks.com", "inquiry"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[] { req });

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.ddtekno.soap.e2pay.WsbgResponse) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.ddtekno.soap.e2pay.WsbgResponse) org.apache.axis.utils.JavaUtils.convert(_resp,
							com.ddtekno.soap.e2pay.WsbgResponse.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.ddtekno.soap.e2pay.WsbgResponse payment(com.ddtekno.soap.e2pay.WsbgRequest req)
			throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[1]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("");
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(
				new javax.xml.namespace.QName("http://exposed.webservice.pacarana.infinetworks.com", "payment"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[] { req });

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.ddtekno.soap.e2pay.WsbgResponse) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.ddtekno.soap.e2pay.WsbgResponse) org.apache.axis.utils.JavaUtils.convert(_resp,
							com.ddtekno.soap.e2pay.WsbgResponse.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.ddtekno.soap.e2pay.WsbgResponse purchase(com.ddtekno.soap.e2pay.WsbgRequest req)
			throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[2]);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
		_call.setOperationName(
				new javax.xml.namespace.QName("http://exposed.webservice.pacarana.infinetworks.com", "purchase"));

		setRequestHeaders(_call);
		setAttachments(_call);
        

		try {
			
			java.lang.Object _resp = _call.invoke(new java.lang.Object[] { req });
			System.out.println(_call.getMessageContext().getRequestMessage().getSOAPPartAsString());

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.ddtekno.soap.e2pay.WsbgResponse) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.ddtekno.soap.e2pay.WsbgResponse) org.apache.axis.utils.JavaUtils.convert(_resp,
							com.ddtekno.soap.e2pay.WsbgResponse.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			System.out.println(_call.getMessageContext().getRequestMessage().getSOAPPartAsString());
			throw axisFaultException;
		}
	}
	
	public org.apache.axis.message.SOAPEnvelope purchase(Message msg)
			throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setProperty(org.apache.axis.client.Call.CHECK_MUST_UNDERSTAND, new Boolean(false));
		_call.setOperation(_operations[2]);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(
				new javax.xml.namespace.QName("http://exposed.webservice.pacarana.infinetworks.com", "purchase"));

		setRequestHeaders(_call);
		setAttachments(_call);
        

		try {
			
			java.lang.Object _resp = _call.invoke(msg);
			System.out.println(_call.getMessageContext().getRequestMessage().getSOAPPartAsString());

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				return (org.apache.axis.message.SOAPEnvelope) _resp;
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			System.out.println(_call.getMessageContext().getRequestMessage().getSOAPPartAsString());
			throw axisFaultException;
		}
	}
	
	public org.apache.axis.message.SOAPEnvelope checkPurchase(Message msg)
			throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setProperty(org.apache.axis.client.Call.CHECK_MUST_UNDERSTAND, new Boolean(false));
		_call.setOperation(_operations[2]);
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(
				new javax.xml.namespace.QName("http://exposed.webservice.pacarana.infinetworks.com", "checkPurchase"));

		setRequestHeaders(_call);
		setAttachments(_call);
        

		try {
			
			java.lang.Object _resp = _call.invoke(msg);
			System.out.println(_call.getMessageContext().getRequestMessage().getSOAPPartAsString());

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				return (org.apache.axis.message.SOAPEnvelope) _resp;
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			System.out.println(_call.getMessageContext().getRequestMessage().getSOAPPartAsString());
			throw axisFaultException;
		}
	}

	public com.ddtekno.soap.e2pay.WsbgAccountLedgerResponse getAccountLedger(
			com.ddtekno.soap.e2pay.WsbgAccountLedgerRequest req) throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[3]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("");
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://exposed.webservice.pacarana.infinetworks.com",
				"getAccountLedger"));
		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			
	        java.lang.Object _resp = _call.invoke(new java.lang.Object[] { req });

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.ddtekno.soap.e2pay.WsbgAccountLedgerResponse) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.ddtekno.soap.e2pay.WsbgAccountLedgerResponse) org.apache.axis.utils.JavaUtils
							.convert(_resp, com.ddtekno.soap.e2pay.WsbgAccountLedgerResponse.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		} 
	}

	public com.ddtekno.soap.e2pay.WsbgClientDepositResponse getDepositBalance(com.ddtekno.soap.e2pay.WsbgRequest req)
			throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[4]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("");
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://exposed.webservice.pacarana.infinetworks.com",
				"getDepositBalance"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[] { req });

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.ddtekno.soap.e2pay.WsbgClientDepositResponse) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.ddtekno.soap.e2pay.WsbgClientDepositResponse) org.apache.axis.utils.JavaUtils
							.convert(_resp, com.ddtekno.soap.e2pay.WsbgClientDepositResponse.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.ddtekno.soap.e2pay.WsbgResponse checkPayment(com.ddtekno.soap.e2pay.WsbgRequest req)
			throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[5]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("");
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(
				new javax.xml.namespace.QName("http://exposed.webservice.pacarana.infinetworks.com", "checkPayment"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[] { req });

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.ddtekno.soap.e2pay.WsbgResponse) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.ddtekno.soap.e2pay.WsbgResponse) org.apache.axis.utils.JavaUtils.convert(_resp,
							com.ddtekno.soap.e2pay.WsbgResponse.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.ddtekno.soap.e2pay.WsbgResponse checkPurchase(com.ddtekno.soap.e2pay.WsbgRequest req)
			throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[6]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("");
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(
				new javax.xml.namespace.QName("http://exposed.webservice.pacarana.infinetworks.com", "checkPurchase"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[] { req });

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.ddtekno.soap.e2pay.WsbgResponse) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.ddtekno.soap.e2pay.WsbgResponse) org.apache.axis.utils.JavaUtils.convert(_resp,
							com.ddtekno.soap.e2pay.WsbgResponse.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.ddtekno.soap.e2pay.WsbgPrepaidProductResponse prepaidProduct(com.ddtekno.soap.e2pay.WsbgRequest req)
			throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[7]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("");
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(
				new javax.xml.namespace.QName("http://exposed.webservice.pacarana.infinetworks.com", "prepaidProduct"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[] { req });

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.ddtekno.soap.e2pay.WsbgPrepaidProductResponse) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.ddtekno.soap.e2pay.WsbgPrepaidProductResponse) org.apache.axis.utils.JavaUtils
							.convert(_resp, com.ddtekno.soap.e2pay.WsbgPrepaidProductResponse.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

	public com.ddtekno.soap.e2pay.WsbgPostpaidProductResponse postpaidProduct(com.ddtekno.soap.e2pay.WsbgRequest req)
			throws java.rmi.RemoteException {
		if (super.cachedEndpoint == null) {
			throw new org.apache.axis.NoEndPointException();
		}
		org.apache.axis.client.Call _call = createCall();
		_call.setOperation(_operations[8]);
		_call.setUseSOAPAction(true);
		_call.setSOAPActionURI("");
		_call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
		_call.setOperationName(new javax.xml.namespace.QName("http://exposed.webservice.pacarana.infinetworks.com",
				"postpaidProduct"));

		setRequestHeaders(_call);
		setAttachments(_call);
		try {
			java.lang.Object _resp = _call.invoke(new java.lang.Object[] { req });

			if (_resp instanceof java.rmi.RemoteException) {
				throw (java.rmi.RemoteException) _resp;
			} else {
				extractAttachments(_call);
				try {
					return (com.ddtekno.soap.e2pay.WsbgPostpaidProductResponse) _resp;
				} catch (java.lang.Exception _exception) {
					return (com.ddtekno.soap.e2pay.WsbgPostpaidProductResponse) org.apache.axis.utils.JavaUtils
							.convert(_resp, com.ddtekno.soap.e2pay.WsbgPostpaidProductResponse.class);
				}
			}
		} catch (org.apache.axis.AxisFault axisFaultException) {
			throw axisFaultException;
		}
	}

}
